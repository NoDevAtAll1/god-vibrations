﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadZone : MonoBehaviour {
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name.Contains("PlayerBall"))
        {
            //Relancer le bard en up !
            other.GetComponent<Rigidbody2D>().AddForce(transform.up * Random.Range(0, 50));
        }
        else if (other.name.Contains("Player"))
        {
            //Kill player ;
            if (other.gameObject.GetComponent<PlayerLife>() != null)
            {
                other.gameObject.GetComponent<PlayerLife>().lancerMort();
            }
        }
		else if (other.name.Contains("pointe"))
		{
			Destroy(other.gameObject);
		}
		}
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.name.Contains("PlayerBall"))
        {
            //Relancer le bard en up !
            other.GetComponent<Rigidbody2D>().AddForce(transform.up * Random.Range(0, 50));
        }
    }
}

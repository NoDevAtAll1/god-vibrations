﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiqueTop : MonoBehaviour
{
    private float _respawnPointeTime = 4f;
    private float _time = 0f;
    private GestionPiege _gestionPiege;
    void Start()
    {
        _gestionPiege = GameObject.Find("World").GetComponent<GestionPiege>();
    }
    void Update()
    {
        if (_time != 0f)
        {
            //print(time);
            _time -= Time.deltaTime;
            if (_time < 0f)
            {
                _time = 0f;
            }
            if (_time == 0f)
            {
                //print("create Pique");
                createPique();
            }
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.name.Contains("Player"))
        {
            //print("Lacher les piques !");
            foreach (Transform child in this.transform)
            {
                if(child.name.Contains("pointe") && child.gameObject.GetComponent<Rigidbody2D>() == null)
                {
                    child.gameObject.AddComponent<Rigidbody2D>();
                }
            }
            _time = _respawnPointeTime;
        }
    }
    private void createPique()
    {
        //print(this.transform.childCount);
        if(this.transform.childCount == 3)
        {
            for (int i = 1; i < 4; i++)
            {
                GameObject unePointe = Instantiate(_gestionPiege.PrefabsPiquePointe, this.transform) as GameObject;
                unePointe.name = "pointe";
                unePointe.transform.position = this.transform.FindChild("spawn" + i).position;
            }
        }
    }
}

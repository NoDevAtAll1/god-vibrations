﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BumpersPiege : MonoBehaviour {
    
	private bool clignotement = false;

	public void lancerDisparition()
    {
        clignotement = true;
		if(this.isActiveAndEnabled)
		{
			StartCoroutine(iClignotement(0.07f));
        	Invoke("disparition", 1.5f);
		}
    }
    
	private void disparition()
    {
        //Arreter clignotement :
        clignotement = false;
        Color maCouleur = new Color(this.GetComponent<SpriteRenderer>().color.r, this.GetComponent<SpriteRenderer>().color.g, this.GetComponent<SpriteRenderer>().color.b, 1);
        this.GetComponent<SpriteRenderer>().color = maCouleur;
        this.gameObject.SetActive(false);
    }
    
	private IEnumerator iClignotement(float waitTime)
    {
        while (clignotement)
        {
            yield return new WaitForSeconds(waitTime);
            //print("WaitAndPrint " + Time.time);
            if (this.GetComponent<SpriteRenderer>().color.a == 0)
            {
                Color maCouleur = new Color(this.GetComponent<SpriteRenderer>().color.r, this.GetComponent<SpriteRenderer>().color.g, this.GetComponent<SpriteRenderer>().color.b, 1);
                this.GetComponent<SpriteRenderer>().color = maCouleur;
            }
            else if(clignotement)
            {
                Color maCouleur = new Color(this.GetComponent<SpriteRenderer>().color.r, this.GetComponent<SpriteRenderer>().color.g, this.GetComponent<SpriteRenderer>().color.b, 0);
                this.GetComponent<SpriteRenderer>().color = maCouleur;
            }
        }
    }
}

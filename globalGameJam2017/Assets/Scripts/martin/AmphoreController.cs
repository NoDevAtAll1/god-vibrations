﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmphoreController : MonoBehaviour {
    private bool _enMouvement = false;
    public float vitesseDeplacement = 5f;
    private Vector3 _positionArriver;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (_enMouvement)
        {
            this.transform.position = Vector2.Lerp(this.GetComponent<Transform>().position, _positionArriver, (Time.fixedDeltaTime * vitesseDeplacement));
            if (this.transform.position.x >= _positionArriver.x-2 || this.transform.position.x <= _positionArriver.x + 2 && this.transform.position.y == 0)
            {
                _enMouvement = false;
                //print("finish");
                //exploserAmphore();
            }
        }
    }
    public void startDeplacement(Vector3 monVector)
    {
        //print("Mouvement lancer");
        _positionArriver = monVector;
        _enMouvement = true;
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class PlayerLife : MonoBehaviour
{
    #region Attributes
    public int vie = 3;
    public int vieMax = 3;
    public bool enVie = true;

    public GameObject[] hearts;
    public GameObject bubbleShield;
    public PlayerControllerMartin controller;

    public AudioSource _sonVieGagnee;
    public AudioSource _sonViePerdue;
    public AudioSource _sonMort;

    private bool _hit = false;
    private bool _invincible = false;

    private SpriteRenderer _renderer;

    #endregion

    #region LifePoints, Damage and Death
    // ---------------------------------------------------
    ///<summary>
    /// Start
    ///</summary>
    void Start()
    {
        this._renderer = this.GetComponent<SpriteRenderer>();
        this.controller = this.GetComponent<PlayerControllerMartin>();
    }

    // ---------------------------------------------------
    ///<summary>
    /// Inflicts damage to the player
    ///</summary>
    public void playerDommage(int domage)
    {
        if (enVie && !this._invincible)
        {
            if (vie != 0 && !_hit)
            {
                if (this._sonViePerdue) this._sonViePerdue.Play();
                vie -= domage;
                this.hearts[vie].SetActive(false);
                _hit = true;
                this.GetComponent<PlayerControllerMartin>().setAnimHit();
                //StartCoroutine(iClignotement(0.1f));
                //_time = 3f;
                Invoke("joueurVisible", 1f);
            }
            if (vie == 0)
            {
                _hit = false;
                //joueurVisible();
                lancerMort();
            }
        }
    }

    // ---------------------------------------------------
    ///<summary>
    /// Blink animation
    ///</summary>
    private IEnumerator iClignotement(float waitTime)
    {
        while (_hit)
        {
            yield return new WaitForSeconds(waitTime);
            //print("WaitAndPrint " + Time.time);
            if (this._renderer.color.a == 0)
            {
                Color color = this._renderer.color;
                color.a = 1;
                this._renderer.color = color;
            }
            else if(_hit)
            {
                //print("0");
                Color color = this._renderer.color;
                color.a = 0;
                this._renderer.color = color;
            }
        }
    }

    // ---------------------------------------------------
    ///<summary>
    /// Launch Player Death
    ///</summary>
    public void lancerMort()
    {
        //print("JoueurMort");
        this.GetComponent<PlayerControllerMartin>().setAnimDeath();
        if (this._sonMort) this._sonMort.Play();
        vie = 0;
        this.transform.FindChild("Life").transform.GetChild((0)).gameObject.SetActive(false);
        this.transform.FindChild("Life").transform.GetChild((1)).gameObject.SetActive(false);
        this.transform.FindChild("Life").transform.GetChild((2)).gameObject.SetActive(false);
        enVie = false;
		//GameObject.Find ("World").GetComponent<GameManager> ().GestionControleFin (gameObject.GetComponent<PlayerControllerMartin>().idJoueur);
    }

    // ---------------------------------------------------
    ///<summary>
    /// Make Player visible
    ///</summary>
    public void joueurVisible()
    {
        //print("Joueur Visible");
        _hit = false;
        //Color maCouleur = new Color(this.GetComponent<SpriteRenderer>().color.r, this.GetComponent<SpriteRenderer>().color.g, this.GetComponent<SpriteRenderer>().color.b, 1);
        //this.GetComponent<SpriteRenderer>().color = maCouleur;
    }
    #endregion

    #region Bonus
    // ---------------------------------------------------
    ///<summary>
    /// SetBonus
    ///</summary>
    public void SetBonus(Item.ItemType type)
    {
        switch (type)
        {
            //-----------------------
            //  BUBBLE
            case Item.ItemType.BUBBLE:
                this.StartCoroutine(this._ActiveBubble());
                break;

            //-----------------------
            //  INVINCIBILITY
            case Item.ItemType.INVINCIBILITY:
                this.StartCoroutine(this._SetInvincible());
                break;

            //-----------------------
            //  INVERSION
            case Item.ItemType.INVERSION:
                this.StartCoroutine(this._ReverseControls());
                break;

            //-----------------------
            //  FREEZE
            case Item.ItemType.FREEZE:
                this.StartCoroutine(this._Freeze());
                break;

            //-----------------------
            //  LIFE
            case Item.ItemType.LIFE:
                this.AddVie();
                break;

        }
    }



    // ---------------------------------------------------
    ///<summary>
    /// Active Bubble
    ///</summary>
    private IEnumerator _ActiveBubble()
    {
        this._invincible = true;
        this.bubbleShield.gameObject.SetActive(true);
        yield return new WaitForSeconds(5.0f);
        this.bubbleShield.gameObject.SetActive(false);
        this._invincible = false;
    }

    // ---------------------------------------------------
    ///<summary>
    /// Reverse Controls
    ///</summary>
    private IEnumerator _ReverseControls()
    {
        this.controller.reverse = true;
		this.transform.FindChild("effetReverse").gameObject.SetActive(true);
        yield return new WaitForSeconds(3.0f);
		this.transform.FindChild("effetReverse").gameObject.SetActive(false);
        this.controller.reverse = false;
    }

    // ---------------------------------------------------
    ///<summary>
    /// Invincibility
    ///</summary>
    private IEnumerator _SetInvincible()
    {

        this._invincible = true;
		this.transform.FindChild("effetImmortal").gameObject.SetActive(true);
        //Color color = new Color(1f, 1f, 1f);
        float time = Time.time;
        //int sequence = 0;
        while (Time.time - time < 4.0f)
        {
            /*if (sequence == 0)
            {
                color.g -= 0.05f;
                color.b -= 0.05f;
                if(color.g <= 0f)
                {
                    color.g -= 0f;
                    color.b -= 0f;
                    sequence++;
                }
                this._renderer.color = color;
            }
            else if (sequence == 1)
            {
                color.g += 0.05f;
                if (color.g >= 1f)
                {
                    sequence++;
                }
                this._renderer.color = color;
            }
            else if (sequence == 2)
            {
                color.g -= 0.05f;
                if (color.g <= 0f)
                {
                    sequence--;
                }
                this._renderer.color = color;
            }*/
            yield return null;
        }
        //this._renderer.color = new Color(1f,1f,1f,1f);
		this.transform.FindChild("effetImmortal").gameObject.SetActive(false);
        this._invincible = false;

    }

    // ---------------------------------------------------
    ///<summary>
    /// Freeze
    ///</summary>
    private IEnumerator _Freeze()
    {
        this.controller.enabled = false;
		this.transform.FindChild("effetFreeze").gameObject.SetActive(true);
        yield return new WaitForSeconds(3.0f);
		this.transform.FindChild("effetFreeze").gameObject.SetActive(false);
        this.controller.enabled = true;
    }

    // ---------------------------------------------------
    ///<summary>
    /// Blink animation
    ///</summary>
    private void AddVie()
    {
        if (!this.enVie) return;
        this.hearts[vie].SetActive(true);
        this.vie++;
        if (vie > vieMax) vie = vieMax;
        if (this._sonVieGagnee) this._sonVieGagnee.Play();
    }
    #endregion

}

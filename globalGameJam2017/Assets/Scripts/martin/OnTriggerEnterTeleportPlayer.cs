﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerEnterTeleportPlayer : MonoBehaviour {
    public GameObject teleportFin;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name.Contains("Player"))
        {
            other.gameObject.transform.position = new Vector3(teleportFin.transform.position.x, other.gameObject.transform.position.y, 0f);
			if(other.gameObject.GetComponent<Rigidbody2D>().velocity.x > 6) {
				other.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(6, 0, 0);
			}
			else if(other.gameObject.GetComponent<Rigidbody2D>().velocity.x < -6) {
				other.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(-6, 0, 0);
			}
        }
    }
}

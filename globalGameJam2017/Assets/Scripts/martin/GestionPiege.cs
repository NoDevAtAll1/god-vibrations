﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestionPiege : MonoBehaviour {
    public List<GameObject> spawnPique;
    public int nbrPiqueMax = 4;
    public int nbrPiqueMin = 1;
    public int nbrPiqueChoisi = 0;
    private List<int> _idSpawnPiqueOccuper = new List<int>();
    public GameObject ParentPiege;
    public Bumpers bumpersDeLaScene;
    //Prefabs :
    public GameObject PrefabsPique;
    public GameObject PrefabsPiquePointe;
    //Gestion des trous :
    public float rateTrou = 20f;
    private int[] _trouSelectionner = new int[2] { -1, -1 };
	private int[] _trouSelectionnerExtraTime = new int[8] { -1, -1, -1, -1, -1, -1, -1, -1 };
    //Gestion du barde :
    public GameObject monBardeAmphore;
    public float bardeTimeApparitionMin = 30f;
    public float bardeTimeApparitionMax = 90f;
    private float bardeTimeSelect = 0f;
	private bool modeExtraTime;

    void Awake()
    {
        bardeTimeSelect = Random.Range(bardeTimeApparitionMin, bardeTimeApparitionMax);
        //print(bardeTimeSelect);
    }
    // Use this for initialization
    void Start()
	{
		modeExtraTime = false;
		nbrPiqueChoisi = Random.Range(nbrPiqueMin, nbrPiqueMax);
        if (nbrPiqueChoisi > spawnPique.Count)
        {
            nbrPiqueChoisi = spawnPique.Count;
        }
        //StartCreatePique();
        InvokeRepeating("createTrou", rateTrou, rateTrou);
        //Invoke("lancerBarde", bardeTimeSelect);
        //print(bardeTimeSelect);
    }
	
    public void StartCreatePique()
    {
        for(int i = 0; i < nbrPiqueChoisi; i++)
        {
            //Trouver le spawn du pique :
            int idTemp = 0;
            bool choixNewId = true;
            while (choixNewId)
            {
                idTemp = Random.Range(0, spawnPique.Count);
                if (!_idSpawnPiqueOccuper.Contains(idTemp))
                {
                    choixNewId = false;
                    _idSpawnPiqueOccuper.Add(idTemp);
                }
            }
            //Creation du pique:
            GameObject tempObject = Instantiate(PrefabsPique, ParentPiege.transform) as GameObject;
            tempObject.name = "Pique";
            tempObject.transform.position = spawnPique[idTemp].transform.position;
        }
    }
    private void createTrou()
    {
		if(!modeExtraTime)
		{
			print("createTrou");
			//Réafficher les bumpers disparue :
	        if (_trouSelectionner[0] != -1)
	        {
	            //print(_trouSelectionner[0]);
	            //print(_trouSelectionner[1]);
	            bumpersDeLaScene.list[_trouSelectionner[0]].SetActive(true);
	            bumpersDeLaScene.list[_trouSelectionner[1]].SetActive(true);
	            _trouSelectionner[0] = -1;
	            _trouSelectionner[1] = -1;
	        }
	        //Set les prochains bumbers à disparaitre :
	        _trouSelectionner[0] = Random.Range(0, bumpersDeLaScene.list.Count - 1);
	        _trouSelectionner[1] = _trouSelectionner[0] + 1;
	        bumpersDeLaScene.list[_trouSelectionner[0]].GetComponent<BumpersPiege>().lancerDisparition();
	        bumpersDeLaScene.list[_trouSelectionner[1]].GetComponent<BumpersPiege>().lancerDisparition();
		}
    }

	private void createTrou2()
	{
		//Réafficher les bumpers disparue :
		if(_trouSelectionner[0] != -1)
		{
			bumpersDeLaScene.list[_trouSelectionner[0]].SetActive(true);
			bumpersDeLaScene.list[_trouSelectionner[1]].SetActive(true);
			_trouSelectionner[0] = -1;
			_trouSelectionner[1] = -1;
		}
		if(_trouSelectionnerExtraTime[0] != -1)
		{
			bumpersDeLaScene.list[_trouSelectionnerExtraTime[0]].SetActive(true);
			bumpersDeLaScene.list[_trouSelectionnerExtraTime[1]].SetActive(true);
			bumpersDeLaScene.list[_trouSelectionnerExtraTime[2]].SetActive(true);
			bumpersDeLaScene.list[_trouSelectionnerExtraTime[3]].SetActive(true);
			bumpersDeLaScene.list[_trouSelectionnerExtraTime[4]].SetActive(true);
			bumpersDeLaScene.list[_trouSelectionnerExtraTime[5]].SetActive(true);
			bumpersDeLaScene.list[_trouSelectionnerExtraTime[6]].SetActive(true);
			bumpersDeLaScene.list[_trouSelectionnerExtraTime[7]].SetActive(true);
			_trouSelectionnerExtraTime[0] = -1;
			_trouSelectionnerExtraTime[1] = -1;
			_trouSelectionnerExtraTime[2] = -1;
			_trouSelectionnerExtraTime[3] = -1;
			_trouSelectionnerExtraTime[4] = -1;
			_trouSelectionnerExtraTime[5] = -1;
			_trouSelectionnerExtraTime[6] = -1;
			_trouSelectionnerExtraTime[7] = -1;
		}
		//Set les prochains bumbers à disparaitre :
		_trouSelectionnerExtraTime[0] = Random.Range(0, bumpersDeLaScene.list.Count - 8);
		_trouSelectionnerExtraTime[1] = _trouSelectionnerExtraTime[0] + 1;
		_trouSelectionnerExtraTime[2] = _trouSelectionnerExtraTime[1] + 1;
		_trouSelectionnerExtraTime[3] = _trouSelectionnerExtraTime[2] + 1;
		_trouSelectionnerExtraTime[4] = _trouSelectionnerExtraTime[3] + 1;
		_trouSelectionnerExtraTime[5] = _trouSelectionnerExtraTime[4] + 1;
		_trouSelectionnerExtraTime[6] = _trouSelectionnerExtraTime[5] + 1;
		_trouSelectionnerExtraTime[7] = _trouSelectionnerExtraTime[6] + 1;
		bumpersDeLaScene.list[_trouSelectionnerExtraTime[0]].GetComponent<BumpersPiege>().lancerDisparition();
		bumpersDeLaScene.list[_trouSelectionnerExtraTime[1]].GetComponent<BumpersPiege>().lancerDisparition();
		bumpersDeLaScene.list[_trouSelectionnerExtraTime[2]].GetComponent<BumpersPiege>().lancerDisparition();
		bumpersDeLaScene.list[_trouSelectionnerExtraTime[3]].GetComponent<BumpersPiege>().lancerDisparition();
		bumpersDeLaScene.list[_trouSelectionnerExtraTime[4]].GetComponent<BumpersPiege>().lancerDisparition();
		bumpersDeLaScene.list[_trouSelectionnerExtraTime[5]].GetComponent<BumpersPiege>().lancerDisparition();
		bumpersDeLaScene.list[_trouSelectionnerExtraTime[6]].GetComponent<BumpersPiege>().lancerDisparition();
		bumpersDeLaScene.list[_trouSelectionnerExtraTime[7]].GetComponent<BumpersPiege>().lancerDisparition();
	}

	public void createTrouExtraTime()
	{
		CancelInvoke("createTrou");
		modeExtraTime = true;
		InvokeRepeating("createTrou2", 1, 5);
	}

    private void lancerBarde()
    {
        //print("Barde lancer");
        monBardeAmphore.GetComponent<BardeController>().startDeplacement();
    }
	
}

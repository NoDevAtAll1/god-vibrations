﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayersManager : MonoBehaviour
{
    public GameObject playerPanel1;
    public GameObject playerPanel2;
    public GameObject playerPanel3;
    public GameObject playerPanel4;
    public int SlotMax = 4;
    public GameObject ecranPerso;
    public GameObject ecranStage;
    public GameObject ecranTuto;
	public GameObject ecranTuto2;
    public GameObject ecranMode;
	public GameObject ecranMode2;
    public bool microActif = false;
    public GameObject cmdOffMicrophone;
	public GameObject PanelNbrGoals;
	private bool stageSelec;
    ////////////////////////////////////////////////////////////
    private int _stateChoose = 0;//0 selection des personnages
    private bool _resetStick1 = true;
    private bool _resetStick2 = true;
    private bool _resetStick3 = true;
    private bool _resetStick4 = true;
    private bool _resetStick5 = true;
    private bool _resetStick6 = true;
    private bool _resetStick7 = true;
    private bool _resetStick8 = true;
    //private int _nbrPlayer = 0;
    /*private bool _Joystick1 = false; 1
    private bool _Joystick2 = false; 2
    private bool _Joystick3 = false; 3
    private bool _Joystick4 = false;  4
    private bool _KeyBoard1 = false; 5
    private bool _KeyBoard2 = false; 6
    private bool _KeyBoard3 = false; 7
    private bool _KeyBoard4 = false; 8*/
    private List<PlayerControllerIdentifier> _lesControllers = new List<PlayerControllerIdentifier>();//controller ajouter
    // Use this for initialization
    void Start()
    {
        variablesGlobales.warriorP1 = 1;
        variablesGlobales.warriorP2 = 1;
        variablesGlobales.warriorP3 = 1;
        variablesGlobales.warriorP4 = 1;
        variablesGlobales.lesControllersActifs = new List<PlayerControllerIdentifier>();
		stageSelec = false;
		//Volume audio
		GameObject.Find("Main Camera").transform.FindChild("Fx").GetComponent<AudioSource>().volume = variablesGlobales.volumeFx;
    }

    // Update is called once per frame
    void Update()
    {
        //
        //SUBMIT
        //
        //Joystick 1 :
        if (Input.GetButtonUp("Submit"))
        {
            submitManagerHeroPanel(1);
        }
        //Joystick 2 :
        if (Input.GetButtonUp("Submit2"))
        {
            submitManagerHeroPanel(2);
        }
        //Joystick 3 :
        if (Input.GetButtonUp("Submit3"))
        {
            submitManagerHeroPanel(3);
        }
        //Joystick 4 :
        if (Input.GetButtonUp("Submit4"))
        {
            submitManagerHeroPanel(4);
        }
        //////////////////////////////////////////////
        //Keyboard 1 :
        if (Input.GetButtonUp("Submit5"))
        {
            submitManagerHeroPanel(5);
        }
        //Keyboard 2 :
        if (Input.GetButtonUp("Submit6"))
        {
            submitManagerHeroPanel(6);
        }
        //Keyboard 3 :
        if (Input.GetButtonUp("Submit7"))
        {
            submitManagerHeroPanel(7);
        }
        //Keyboard 4 :
        if (Input.GetButtonUp("Submit8"))
        {
            submitManagerHeroPanel(8);
        }
        //
        //Cancel :
        //
        //Joystick 1 :
        if (Input.GetButtonUp("Cancel"))
        {
            cancelManagerHeroPanel(1);
        }
        //Joystick 2 :
        if (Input.GetButtonUp("Cancel2"))
        {
            cancelManagerHeroPanel(2);
        }
        //Joystick 3 :
        if (Input.GetButtonUp("Cancel3"))
        {
            cancelManagerHeroPanel(3);
        }
        //Joystick 4 :
        if (Input.GetButtonUp("Cancel4"))
        {
            cancelManagerHeroPanel(4);
        }
        //////////////////////////////////////////////
        //Keyboard 1 :
        if (Input.GetButtonUp("Cancel5"))
        {
            cancelManagerHeroPanel(5);
        }
        //Keyboard 2 :
        if (Input.GetButtonUp("Cancel6"))
        {
            cancelManagerHeroPanel(6);
        }
        //Keyboard 3 :
        if (Input.GetButtonUp("Cancel7"))
        {
            cancelManagerHeroPanel(7);
        }
        //Keyboard 4 :
        if (Input.GetButtonUp("Cancel8"))
        {
            cancelManagerHeroPanel(8);
        }
        //
        //LEFT OR RIGHT :
        //
        //Joystick 1 :
        if (Input.GetAxis("Horizontal") < 0 && _resetStick1)
        {
            _resetStick1 = false;
            leftManagerHeroPanel(1);
        }
        else if (Input.GetAxis("Horizontal") > 0 && _resetStick1)
        {
            _resetStick1 = false;
            rightManagerHeroPanel(1);
        }
        else if (Input.GetAxis("Horizontal") == 0 && !_resetStick1)
        {
            _resetStick1 = true;
        }
        //Joystick 2 :
        if (Input.GetAxis("Horizontal2") < 0 && _resetStick2)
        {
            _resetStick2 = false;
            leftManagerHeroPanel(2);
        }
        else if (Input.GetAxis("Horizontal2") > 0 && _resetStick2)
        {
            _resetStick2 = false;
            rightManagerHeroPanel(2);
        }
        else if (Input.GetAxis("Horizontal2") == 0 && !_resetStick2)
        {
            _resetStick2 = true;
        }
        //Joystick 3 :
        if (Input.GetAxis("Horizontal3") < 0 && _resetStick3)
        {
            _resetStick3 = false;
            leftManagerHeroPanel(3);
        }
        else if (Input.GetAxis("Horizontal3") > 0 && _resetStick3)
        {
            _resetStick3 = false;
            rightManagerHeroPanel(3);
        }
        else if (Input.GetAxis("Horizontal3") == 0 && !_resetStick3)
        {
            _resetStick3 = true;
        }
        //Joystick 4 :
        if (Input.GetAxis("Horizontal4") < 0 && _resetStick4)
        {
            _resetStick4 = false;
            leftManagerHeroPanel(4);
        }
        else if (Input.GetAxis("Horizontal4") > 0 && _resetStick4)
        {
            _resetStick4 = false;
            rightManagerHeroPanel(4);
        }
        else if (Input.GetAxis("Horizontal4") == 0 && !_resetStick4)
        {
            _resetStick4 = true;
        }
        ////////////////////////////////////////////////////////////////
        //Keyboard 1 :
        if (Input.GetAxis("Horizontal5") < 0 && _resetStick5)
        {
            _resetStick5 = false;
            leftManagerHeroPanel(5);
        }
        else if (Input.GetAxis("Horizontal5") > 0 && _resetStick5)
        {
            _resetStick5 = false;
            rightManagerHeroPanel(5);
        }
        else if (Input.GetAxis("Horizontal5") == 0 && !_resetStick5)
        {
            _resetStick5 = true;
        }
        //Keyboard 2 :
        if (Input.GetAxis("Horizontal6") < 0 && _resetStick6)
        {
            _resetStick6 = false;
            leftManagerHeroPanel(6);
        }
        else if (Input.GetAxis("Horizontal6") > 0 && _resetStick6)
        {
            _resetStick6 = false;
            rightManagerHeroPanel(6);
        }
        else if (Input.GetAxis("Horizontal6") == 0 && !_resetStick6)
        {
            _resetStick6 = true;
        }
        //Keyboard 3 :
        if (Input.GetAxis("Horizontal7") < 0 && _resetStick7)
        {
            _resetStick7 = false;
            leftManagerHeroPanel(7);
        }
        else if (Input.GetAxis("Horizontal7") > 0 && _resetStick7)
        {
            _resetStick7 = false;
            rightManagerHeroPanel(7);
        }
        else if (Input.GetAxis("Horizontal7") == 0 && !_resetStick7)
        {
            _resetStick7 = true;
        }
        //Keyboard 4 :
        if (Input.GetAxis("Horizontal8") < 0 && _resetStick8)
        {
            _resetStick8 = false;
            leftManagerHeroPanel(8);
        }
        else if (Input.GetAxis("Horizontal8") > 0 && _resetStick8)
        {
            _resetStick8 = false;
            rightManagerHeroPanel(8);
        }
        else if (Input.GetAxis("Horizontal8") == 0 && !_resetStick8)
        {
            _resetStick8 = true;
        }

    }

    private void submitManagerHeroPanel(int unId)
    {
        if (_stateChoose == 0)//Join des personnes et selection des personnages
        {
			if (checkController(unId))
            {
				//Fx
				GameObject.Find("World").GetComponent<soundControl>().JouerFx ("fxBtn1");
				ajouterController(unId);//Rajouter la manette ou clavier :
                checkIfAllReady();
            }
            else if (checkPersoCanChoose(unId))
            {
                validerPersonnage(unId);//Valider la selection du personnage :

            }
            else
            {
                //Vérifier si tous le monde a valider :
                bool allValid = false;
                if (_lesControllers.Count > 1)
                {
                    allValid = true;
                    foreach (PlayerControllerIdentifier unController in _lesControllers)
                    {
                        if (!unController.heroChoose)
                        {
                            allValid = false;
                        }
                    }
                }
                if (allValid)//Si tous le monde a valider son personnage :
                {
					if(variablesGlobales.modeBardBall == false)
					{
						//Fx
						GameObject.Find("World").GetComponent<soundControl>().JouerFx ("fxValider");
						//Lancer la selection de la map !
	                    //print("Selection de la map !");
	                    _stateChoose = 1;//Selection map :
	                    ecranPerso.SetActive(false);
	                    ecranStage.SetActive(true);

	                    ecranStage.transform.FindChild("Stage").FindChild("Validation").gameObject.SetActive(false);
	                    ecranStage.transform.FindChild("Stage").FindChild("Random").gameObject.SetActive(false);
	                    //Visuel stage
	                    variablesGlobales.numStage = 1;
	                    ecranStage.transform.FindChild("Stage").FindChild("Visuel").GetComponent<Image>().color = Color.white;
	                    ecranStage.transform.FindChild("Stage").FindChild("Visuel").GetComponent<Image>().sprite = Resources.Load<Sprite>("VignettesStages/visuel_stage" + variablesGlobales.numStage) as Sprite;
	                    //Texte stage
	                    ecranStage.transform.FindChild("Stage").FindChild("NomStage").GetComponent<Text>().text = "~ " + variablesGlobales.tabTextesStages[0, 0] + " ~";
	                    ecranStage.transform.FindChild("Stage").FindChild("DescriptionStage").GetComponent<Text>().text = "\"" + variablesGlobales.tabTextesStages[0, 1] + "\"";
					}
					else
					{
						//Fx
						GameObject.Find("World").GetComponent<soundControl>().JouerFx ("fxValider");
						_stateChoose = 100;
						ecranPerso.SetActive(false);
						ecranMode2.SetActive(true);
						EventSystem.current.firstSelectedGameObject=PanelNbrGoals.gameObject;
						EventSystem.current.SetSelectedGameObject(PanelNbrGoals.gameObject, new BaseEventData(EventSystem.current));
					  //EventSystem.current.SetSelectedGameObject(cmdOffMicrophone.gameObject, new BaseEventData(EventSystem.current));
					}
                }
            }
        }
		else if(_stateChoose == 1 && !stageSelec)//Selectionne la map :
        {
			if(variablesGlobales.numStage == variablesGlobales.maxStage)
            {
				//Fx
				GameObject.Find("World").GetComponent<soundControl>().JouerFx("fxBtn1");
				variablesGlobales.numStage = (int)Random.Range(1, 6.5f);
                ecranStage.transform.FindChild("Stage").FindChild("Random").gameObject.SetActive(false);
                ecranStage.transform.FindChild("Stage").FindChild("NomStage").GetComponent<Text>().text = "~ " + variablesGlobales.tabTextesStages[variablesGlobales.numStage - 1, 0] + " ~";
                ecranStage.transform.FindChild("Stage").FindChild("DescriptionStage").GetComponent<Text>().text = "\"" + variablesGlobales.tabTextesStages[variablesGlobales.numStage - 1, 1] + "\"";
                ecranStage.transform.FindChild("Stage").FindChild("Visuel").GetComponent<Image>().color = Color.white;
                ecranStage.transform.FindChild("Stage").FindChild("Visuel").GetComponent<Image>().sprite = Resources.Load<Sprite>("VignettesStages/visuel_stage" + variablesGlobales.numStage) as Sprite;
            }
			else
            {
				stageSelec = true;
				//Fx
				GameObject.Find("World").GetComponent<soundControl>().JouerFx("fxValider");
				if(variablesGlobales.numStage == 1) GameObject.Find("World").GetComponent<soundControl>().JouerFx("fxStageEgypt");
				else if(variablesGlobales.numStage == 2) GameObject.Find("World").GetComponent<soundControl>().JouerFx("fxStageAztec");
				else if(variablesGlobales.numStage == 3) GameObject.Find("World").GetComponent<soundControl>().JouerFx("fxStageNordic");
				else if(variablesGlobales.numStage == 4) GameObject.Find("World").GetComponent<soundControl>().JouerFx("fxStageAfrica");
				else if(variablesGlobales.numStage == 5) GameObject.Find("World").GetComponent<soundControl>().JouerFx("fxStageForest");
				else if(variablesGlobales.numStage == 6) GameObject.Find("World").GetComponent<soundControl>().JouerFx("fxStageChickenRune");
				ecranStage.transform.FindChild("Stage").FindChild("Validation").gameObject.SetActive(true);
				Invoke("LancerMenuMode", 2f);
            }

        }
        else if(_stateChoose == 2)//Lancer micro selection :
        {
            _stateChoose = 3;
            ecranMode.SetActive(false);
            ecranTuto.SetActive(true);
			//Fx
			GameObject.Find("World").GetComponent<soundControl>().JouerFx ("fxValider");
            //cmdOffMicrophone.GetComponent<Button>().Select();
			EventSystem.current.firstSelectedGameObject=cmdOffMicrophone.gameObject;
            EventSystem.current.SetSelectedGameObject(cmdOffMicrophone.gameObject, new BaseEventData(EventSystem.current));
        }
        else if(_stateChoose == 3)
        {
            //print("Lancer le jeu");
            //Set des variables globale :
            variablesGlobales.nbrPlayers = _lesControllers.Count;
            variablesGlobales.lesControllersActifs = _lesControllers;
            variablesGlobales.microphoneActiver = microActif;
            //Lancer le jeu :
            LancerCombat();
        }
		else if(_stateChoose == 100)
		{
			_stateChoose = 101;
			ecranMode2.SetActive(false);
			ecranTuto2.SetActive(true);
		}
		else if(_stateChoose == 101)
		{
			//Set des variables globale :
			variablesGlobales.nbrPlayers = _lesControllers.Count;
			variablesGlobales.lesControllersActifs = _lesControllers;
			variablesGlobales.microphoneActiver = microActif;
			//Lancer le jeu :
			LancerBardBall();
		}
    }

	private void LancerMenuMode()
	{
		if(_stateChoose == 1)
		{
			ecranStage.transform.FindChild("Stage").FindChild("Validation").gameObject.SetActive(false);
			_stateChoose = 2;
			ecranStage.SetActive(false);
			ecranMode.SetActive(true);
		}
	}

	public void InitGoalsToWin(int nbrGoals)
	{
		//Fx
		GameObject.Find("World").GetComponent<soundControl>().JouerFx("fxValider");
		variablesGlobales.goalsToWin = nbrGoals;
	}

	public void LancerBardBall()
	{
		//Fx
		GameObject.Find("World").GetComponent<soundControl>().JouerFx("fxValider");
		SceneManager.LoadScene("SceneBardeBall");
	}
    public void LancerCombat()
    {
        variablesGlobales.scoreP1 = 0;
        variablesGlobales.scoreP2 = 0;
        variablesGlobales.scoreP3 = 0;
        variablesGlobales.scoreP4 = 0;
        //print ("Le combat commence !");
        string nameScene = "";
        switch (variablesGlobales.numStage)
        {
            case 1:
                nameScene = "SceneEgypt";
                break;
            case 2:
                nameScene = "SceneAztec";
                break;
            case 3:
                nameScene = "SceneNordic";
                break;
            case 4:
                nameScene = "SceneArfica";
                break;
            case 5:
                nameScene = "SceneBarde";
                break;
            case 6:
                nameScene = "SceneChickenRune";
                break;
            case 7://Random
                int newCase = Random.Range(1, 6);
                variablesGlobales.numStage = newCase;
                LancerCombat();
                break;
        }
        if(nameScene != "")
        {
			//Fx
			GameObject.Find("World").GetComponent<soundControl>().JouerFx("fxValider");
			variablesGlobales.scoreP1 = 0;
            variablesGlobales.scoreP2 = 0;
            variablesGlobales.scoreP3 = 0;
            variablesGlobales.scoreP4 = 0;
            SceneManager.LoadScene(nameScene);
        }
    }
    private void cancelManagerHeroPanel(int unId)
    {
        if(_stateChoose == 0)//Join des personnes et selection des personnages
        {
            if(checkController(unId))
            {
                //Retour au menu principale :
                //print("Retour arriere");
                SceneManager.LoadScene("SceneMenuV2");
            }
            else if(!checkPersoCanChoose(unId))
            {
				//Fx
				GameObject.Find("World").GetComponent<soundControl>().JouerFx ("fxBtn2");
				//Désélectionner la selection du personnage :
                foreach (PlayerControllerIdentifier unController in _lesControllers)
                {
                    if (unController.id == unId)
                    {
                        unController.heroChoose = false;
                        switch (unController.idSlotPlayer)
                        {
                            case 1:
                                playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("Validation").gameObject.SetActive(false);
                                break;
                            case 2:
                                playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("Validation").gameObject.SetActive(false);
                                break;
                            case 3:
                                playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("Validation").gameObject.SetActive(false);
                                break;
                            case 4:
                                playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("Validation").gameObject.SetActive(false);
                                break;
                        }
                    }
                }
                checkIfAllReady();
            }
            else
            {
                //Désélectionner le controller !
                PlayerControllerIdentifier monPlayer = null;
                foreach (PlayerControllerIdentifier unController in _lesControllers)
                {
                    if (unController.id == unId)
                    {
                        monPlayer = unController;
                        switch (unController.idSlotPlayer)
                        {
                            case 1:
                                playerPanel1.transform.FindChild("PanelHaveJoin").gameObject.SetActive(false);
                                playerPanel1.transform.FindChild("PanelNeedJoin").gameObject.SetActive(true);
                                break;
                            case 2:
                                playerPanel2.transform.FindChild("PanelHaveJoin").gameObject.SetActive(false);
                                playerPanel2.transform.FindChild("PanelNeedJoin").gameObject.SetActive(true);
                                break;
                            case 3:
                                playerPanel3.transform.FindChild("PanelHaveJoin").gameObject.SetActive(false);
                                playerPanel3.transform.FindChild("PanelNeedJoin").gameObject.SetActive(true);
                                break;
                            case 4:
                                playerPanel4.transform.FindChild("PanelHaveJoin").gameObject.SetActive(false);
                                playerPanel4.transform.FindChild("PanelNeedJoin").gameObject.SetActive(true);
                                break;
                        }

                    }
                }
                if (monPlayer != null)
                {
                    _lesControllers.Remove(monPlayer);
                    checkIfAllReady();
                }
            }
        }
        else if (_stateChoose == 1)//On retourne a la séléction des personnages :
        {
            _stateChoose = 0;
			stageSelec = false;
            ecranStage.SetActive(false);
            ecranPerso.SetActive(true);
			//Fx
			GameObject.Find("World").GetComponent<soundControl>().JouerFx ("fxBtn2");
        }
        else if (_stateChoose == 2)//On retourne a la selection de la map :
        {

            _stateChoose = 1;
            ecranStage.SetActive(true);
            ecranMode.SetActive(false);
			stageSelec = false;
			//Fx
			GameObject.Find("World").GetComponent<soundControl>().JouerFx ("fxBtn2");
        }
        else if (_stateChoose == 3)//On retourne a la séléction du micro :
        {

            _stateChoose = 2;
            ecranTuto.SetActive(false);
            ecranMode.SetActive(true);
			//Fx
			GameObject.Find("World").GetComponent<soundControl>().JouerFx ("fxBtn2");
            //cmdOffMicrophone.GetComponent<Button>().Select();
			EventSystem.current.firstSelectedGameObject=cmdOffMicrophone.gameObject;
            EventSystem.current.SetSelectedGameObject(cmdOffMicrophone.gameObject, new BaseEventData(EventSystem.current));
        }
		else if (_stateChoose == 100)
		{

			_stateChoose = 0;
			ecranPerso.SetActive(true);
			ecranMode2.SetActive(false);
			//Fx
			GameObject.Find("World").GetComponent<soundControl>().JouerFx ("fxBtn2");
		}
		else if (_stateChoose == 101)
		{

			_stateChoose = 100;
			ecranMode2.SetActive(true);
			ecranTuto2.SetActive(false);
			EventSystem.current.firstSelectedGameObject=PanelNbrGoals.gameObject;
			EventSystem.current.SetSelectedGameObject(PanelNbrGoals.gameObject, new BaseEventData(EventSystem.current));
			//Fx
			GameObject.Find("World").GetComponent<soundControl>().JouerFx ("fxBtn2");
		}
    }

	//Voix god
	private void VoixGodselect(int numGod)
	{
		switch(numGod)
		{
			case 1:
				GameObject.Find("World").GetComponent<soundControl>().JouerFx ("fxNomAnubis");
				break;
			case 2:
				GameObject.Find("World").GetComponent<soundControl>().JouerFx ("fxNomHuitzi");
				break;
			case 3:
				GameObject.Find("World").GetComponent<soundControl>().JouerFx ("fxNomJakuta");
				break;
			case 4:
				GameObject.Find("World").GetComponent<soundControl>().JouerFx ("fxNomTyr");
				break;
		}
	}

    private void leftManagerHeroPanel(int unId)
    {
        if (_stateChoose == 0)//Join des personnes et selection des personnages
        {
            if (!checkController(unId))
            {
                if (checkPersoCanChoose(unId))
                {
					//Fx
					GameObject.Find("World").GetComponent<soundControl>().JouerFx ("fxMouv");
					//print("left");
                    int monSlot = getIdSlotWithIdController(unId);
                    switch (monSlot)
                    {
                        case 1:
                            variablesGlobales.warriorP1--;
                            if (variablesGlobales.warriorP1 < 1) variablesGlobales.warriorP1 = variablesGlobales.nbrWarrior;
                            //Nom warrior
                            playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("NomWarrior").GetComponent<Text>().text = variablesGlobales.tabTextesWarriors[variablesGlobales.warriorP1 - 1];
                            //Skin warrior
                            playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("Warrior").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP1) as Sprite;
							break;
                        case 2:
                            variablesGlobales.warriorP2--;
                            if (variablesGlobales.warriorP2 < 1) variablesGlobales.warriorP2 = variablesGlobales.nbrWarrior;
                            //Nom warrior
                            playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("NomWarrior").GetComponent<Text>().text = variablesGlobales.tabTextesWarriors[variablesGlobales.warriorP2 - 1];
                            //Skin warrior
                            playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("Warrior").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP2) as Sprite;   
							break;
                        case 3:
                            variablesGlobales.warriorP3--;
                            if (variablesGlobales.warriorP3 < 1) variablesGlobales.warriorP3 = variablesGlobales.nbrWarrior;
                            //Nom warrior
                            playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("NomWarrior").GetComponent<Text>().text = variablesGlobales.tabTextesWarriors[variablesGlobales.warriorP3 - 1];
                            //Skin warrior
                            playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("Warrior").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP3) as Sprite;
							break;
                        case 4:
                            variablesGlobales.warriorP4--;
                            if (variablesGlobales.warriorP4 < 1) variablesGlobales.warriorP4 = variablesGlobales.nbrWarrior;
                            //Nom warrior
                            playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("NomWarrior").GetComponent<Text>().text = variablesGlobales.tabTextesWarriors[variablesGlobales.warriorP4 - 1];
                            //Skin warrior
                            playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("Warrior").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP4) as Sprite;
							break;
                    }
                }
            }
        }
        else if (_stateChoose == 1)
        {
			//Fx
			GameObject.Find("World").GetComponent<soundControl>().JouerFx ("fxMouv");
			//Selectionne la map :
            variablesGlobales.numStage--;
            if (variablesGlobales.numStage < 1) variablesGlobales.numStage = variablesGlobales.maxStage;
            //Visuel stage
            ecranStage.transform.FindChild("Stage").FindChild("Visuel").GetComponent<Image>().sprite = Resources.Load<Sprite>("VignettesStages/visuel_stage" + variablesGlobales.numStage) as Sprite;
            //Texte stage
            ecranStage.transform.FindChild("Stage").FindChild("NomStage").GetComponent<Text>().text = "~ " + variablesGlobales.tabTextesStages[variablesGlobales.numStage - 1, 0] + " ~";
            ecranStage.transform.FindChild("Stage").FindChild("DescriptionStage").GetComponent<Text>().text = "\"" + variablesGlobales.tabTextesStages[variablesGlobales.numStage - 1, 1] + "\"";
            //Ecran random
            if (variablesGlobales.numStage == variablesGlobales.maxStage)
            {
                ecranStage.transform.FindChild("Stage").FindChild("Random").gameObject.SetActive(true);
                ecranStage.transform.FindChild("Stage").FindChild("Visuel").GetComponent<Image>().sprite = null;
                ecranStage.transform.FindChild("Stage").FindChild("Visuel").GetComponent<Image>().color = Color.black;
            }
            else
            {
                ecranStage.transform.FindChild("Stage").FindChild("Random").gameObject.SetActive(false);
                ecranStage.transform.FindChild("Stage").FindChild("Visuel").GetComponent<Image>().color = Color.white;
            }
        }
    }
    private void rightManagerHeroPanel(int unId)
    {
        if (_stateChoose == 0)//Join des personnes et selection des personnages
        {
            if (!checkController(unId))
            {
                if (checkPersoCanChoose(unId))
                {
					//Fx
					GameObject.Find("World").GetComponent<soundControl>().JouerFx ("fxMouv");
					//print("right");
                    int monSlot = getIdSlotWithIdController(unId);
                    switch (monSlot)
                    {
                        case 1:
                            variablesGlobales.warriorP1++;
                            if (variablesGlobales.warriorP1 > variablesGlobales.nbrWarrior) variablesGlobales.warriorP1 = 1;
                            //Nom warrior
                            playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("NomWarrior").GetComponent<Text>().text = variablesGlobales.tabTextesWarriors[variablesGlobales.warriorP1 - 1];
                            //Skin warrior
                            playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("Warrior").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP1) as Sprite;   
							break;
                        case 2:
                            variablesGlobales.warriorP2++;
                            if (variablesGlobales.warriorP2 > variablesGlobales.nbrWarrior) variablesGlobales.warriorP2 = 1;
                            //Nom warrior
                            playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("NomWarrior").GetComponent<Text>().text = variablesGlobales.tabTextesWarriors[variablesGlobales.warriorP2 - 1];
                            //Skin warrior
                            playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("Warrior").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP2) as Sprite;
							break;
                        case 3:
                            variablesGlobales.warriorP3++;
                            if (variablesGlobales.warriorP3 > variablesGlobales.nbrWarrior) variablesGlobales.warriorP3 = 1;
                            //Nom warrior
                            playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("NomWarrior").GetComponent<Text>().text = variablesGlobales.tabTextesWarriors[variablesGlobales.warriorP3 - 1];
                            //Skin warrior
                            playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("Warrior").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP3) as Sprite;
							break;
                        case 4:
                            variablesGlobales.warriorP4++;
                            if (variablesGlobales.warriorP4 > variablesGlobales.nbrWarrior) variablesGlobales.warriorP4 = 1;
                            //Nom warrior
                            playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("NomWarrior").GetComponent<Text>().text = variablesGlobales.tabTextesWarriors[variablesGlobales.warriorP4 - 1];
                            //Skin warrior
                            playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("Warrior").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP4) as Sprite;
							break;
                    }
                }
            }
        }
        else if (_stateChoose == 1)
        {
			//Fx
			GameObject.Find("World").GetComponent<soundControl>().JouerFx ("fxMouv");
			//Selectionne la map :
            variablesGlobales.numStage++;
            if (variablesGlobales.numStage > variablesGlobales.maxStage) variablesGlobales.numStage = 1;
            //Visuel stage
            ecranStage.transform.FindChild("Stage").FindChild("Visuel").GetComponent<Image>().sprite = Resources.Load<Sprite>("VignettesStages/visuel_stage" + variablesGlobales.numStage) as Sprite;
            //Texte stage
            ecranStage.transform.FindChild("Stage").FindChild("NomStage").GetComponent<Text>().text = "~ " + variablesGlobales.tabTextesStages[variablesGlobales.numStage - 1, 0] + " ~";
            ecranStage.transform.FindChild("Stage").FindChild("DescriptionStage").GetComponent<Text>().text = "\"" + variablesGlobales.tabTextesStages[variablesGlobales.numStage - 1, 1] + "\"";
            //Ecran random
            if (variablesGlobales.numStage == variablesGlobales.maxStage)
            {
                ecranStage.transform.FindChild("Stage").FindChild("Random").gameObject.SetActive(true);
                ecranStage.transform.FindChild("Stage").FindChild("Visuel").GetComponent<Image>().sprite = null;
                ecranStage.transform.FindChild("Stage").FindChild("Visuel").GetComponent<Image>().color = Color.black;
            }
            else
            {
                ecranStage.transform.FindChild("Stage").FindChild("Random").gameObject.SetActive(false);
                ecranStage.transform.FindChild("Stage").FindChild("Visuel").GetComponent<Image>().color = Color.white;
            }

        }
    }
    private void ajouterController(int unId)
    {
        int cetteId = unId;
        int slotPersonnage = 1;
        bool slotNonLibre = true;
        bool slotVide = true;
        while (slotNonLibre)
        {
            slotNonLibre = false;
            foreach (PlayerControllerIdentifier unController in _lesControllers)
            {
                if (unController.idSlotPlayer == slotPersonnage)
                {
                    slotNonLibre = true;
                }
            }
            if (slotNonLibre)
            {
                if (slotPersonnage == SlotMax)
                {
                    slotVide = false;
                    slotNonLibre = false;
                }
                slotPersonnage++;
            }
        }
        if (slotVide)
        {
            //print("Slot :" + slotPersonnage + " Selectionner pour l'id :" + cetteId);
            _lesControllers.Add(new PlayerControllerIdentifier(cetteId, slotPersonnage));
            string txtGaucheCmd = "";
            string txtDroiteCmd = "";
            switch (cetteId)
            {
                case 5:
                    txtGaucheCmd = "Q";
                    txtDroiteCmd = "D";
                    break;
                case 6:
                    txtGaucheCmd = "<-";
                    txtDroiteCmd = "->";
                    break;
                case 7:
                    txtGaucheCmd = "4";
                    txtDroiteCmd = "6";
                    break;
                case 8:
                    txtGaucheCmd = "K";
                    txtDroiteCmd = "M";
                    break;
            }
            switch (slotPersonnage)
            {
                case 1:
                    if (cetteId > 4)//Clavier :
                    {
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("txtIdController").GetComponent<Text>().text = (cetteId-4).ToString();
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("ImgManette").gameObject.SetActive(false);
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("ImgClavier").gameObject.SetActive(true);
                        //Instruction G et D :
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("flecheG").gameObject.SetActive(false);
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("flecheD").gameObject.SetActive(false);
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("toucheG").gameObject.SetActive(true);
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("toucheD").gameObject.SetActive(true);
                        //Gestion du texte des buttons instuctions :
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("toucheG").FindChild("Text").GetComponent<Text>().text= txtGaucheCmd;
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("toucheD").FindChild("Text").GetComponent<Text>().text = txtDroiteCmd;
                    }
                    else//Manette :
                    {
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("txtIdController").GetComponent<Text>().text = cetteId.ToString();
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("ImgManette").gameObject.SetActive(true);
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("ImgClavier").gameObject.SetActive(false);
                        //Instruction G et D :
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("toucheG").gameObject.SetActive(false);
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("toucheD").gameObject.SetActive(false);
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("flecheG").gameObject.SetActive(true);
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("flecheD").gameObject.SetActive(true);
                    }
                    playerPanel1.transform.FindChild("PanelHaveJoin").gameObject.SetActive(true);
                    playerPanel1.transform.FindChild("PanelNeedJoin").gameObject.SetActive(false);
                    break;
                case 2:
                    if (cetteId > 4)//Clavier :
                    {
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("txtIdController").GetComponent<Text>().text = (cetteId - 4).ToString();
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("ImgManette").gameObject.SetActive(false);
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("ImgClavier").gameObject.SetActive(true);
                        //Instruction G et D :
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("flecheG").gameObject.SetActive(false);
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("flecheD").gameObject.SetActive(false);
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("toucheG").gameObject.SetActive(true);
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("toucheD").gameObject.SetActive(true);
                        //Gestion du texte des buttons instuctions :
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("toucheG").FindChild("Text").GetComponent<Text>().text = txtGaucheCmd;
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("toucheD").FindChild("Text").GetComponent<Text>().text = txtDroiteCmd;

                    }
                    else//Manette :
                    {
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("txtIdController").GetComponent<Text>().text = cetteId.ToString();
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("ImgManette").gameObject.SetActive(true);
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("ImgClavier").gameObject.SetActive(false);
                        //CmdInstruction G et D :
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("toucheG").gameObject.SetActive(false);
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("toucheD").gameObject.SetActive(false);
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("flecheG").gameObject.SetActive(true);
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("flecheD").gameObject.SetActive(true);
                    }
                    playerPanel2.transform.FindChild("PanelHaveJoin").gameObject.SetActive(true);
                    playerPanel2.transform.FindChild("PanelNeedJoin").gameObject.SetActive(false);
                    break;
                case 3:
                    if (cetteId > 4)//Clavier :
                    {
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("txtIdController").GetComponent<Text>().text = (cetteId - 4).ToString();
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("ImgManette").gameObject.SetActive(false);
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("ImgClavier").gameObject.SetActive(true);
                        //Instruction G et D :
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("flecheG").gameObject.SetActive(false);
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("flecheD").gameObject.SetActive(false);
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("toucheG").gameObject.SetActive(true);
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("toucheD").gameObject.SetActive(true);
                        //Gestion du texte des buttons instuctions :
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("toucheG").FindChild("Text").GetComponent<Text>().text = txtGaucheCmd;
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("toucheD").FindChild("Text").GetComponent<Text>().text = txtDroiteCmd;
                    }
                    else//Manette :
                    {
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("txtIdController").GetComponent<Text>().text = cetteId.ToString();
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("ImgManette").gameObject.SetActive(true);
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("ImgClavier").gameObject.SetActive(false);
                        //CmdInstruction G et D :
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("toucheG").gameObject.SetActive(false);
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("toucheD").gameObject.SetActive(false);
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("flecheG").gameObject.SetActive(true);
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("flecheD").gameObject.SetActive(true);

                    }
                    playerPanel3.transform.FindChild("PanelHaveJoin").gameObject.SetActive(true);
                    playerPanel3.transform.FindChild("PanelNeedJoin").gameObject.SetActive(false);
                    break;
                case 4:
                    if (cetteId > 4)//Clavier :
                    {
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("txtIdController").GetComponent<Text>().text = (cetteId - 4).ToString();
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("ImgManette").gameObject.SetActive(false);
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("ImgClavier").gameObject.SetActive(true);
                        //Instruction G et D :
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("flecheG").gameObject.SetActive(false);
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("flecheD").gameObject.SetActive(false);
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("toucheG").gameObject.SetActive(true);
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("toucheD").gameObject.SetActive(true);
                        //Gestion du texte des buttons instuctions :
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("toucheG").FindChild("Text").GetComponent<Text>().text = txtGaucheCmd;
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("toucheD").FindChild("Text").GetComponent<Text>().text = txtDroiteCmd;
                    }
                    else//Manette :
                    {
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("txtIdController").GetComponent<Text>().text = cetteId.ToString();
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("ImgManette").gameObject.SetActive(true);
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("ImgClavier").gameObject.SetActive(false);
                        //CmdInstruction G et D :
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("toucheG").gameObject.SetActive(false);
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("toucheD").gameObject.SetActive(false);
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("flecheG").gameObject.SetActive(true);
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("flecheD").gameObject.SetActive(true);
                    }
                    playerPanel4.transform.FindChild("PanelHaveJoin").gameObject.SetActive(true);
                    playerPanel4.transform.FindChild("PanelNeedJoin").gameObject.SetActive(false);
                    break;
            }

        }
        else
        {
            print("Tous les Slots sont plein !");
        }
    }
    private bool checkController(int unId)
    {
        bool pasTrouver = true;
        foreach (PlayerControllerIdentifier unControler in _lesControllers)
        {
            if (unControler.id == unId)
            {
                pasTrouver = false;
            }
        }
        return pasTrouver;
    }
    private bool checkPersoCanChoose(int unId)
    {
        bool pasCheckHero = true;
        foreach (PlayerControllerIdentifier unControler in _lesControllers)
        {
            if (unControler.id == unId)
            {
                if (unControler.heroChoose)
                {
                    pasCheckHero = false;
                }
            }
        }
        return pasCheckHero;
    }
    private void validerPersonnage(int unId)
    {
		foreach (PlayerControllerIdentifier unController in _lesControllers)
        {
            if (unController.id == unId)
            {
                unController.heroChoose = true;
                switch (unController.idSlotPlayer)
                {
                    case 1:
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("Validation").gameObject.SetActive(true);
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(true);
						VoixGodselect(variablesGlobales.warriorP1);
                        break;
                    case 2:
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("Validation").gameObject.SetActive(true);
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(true);
						VoixGodselect(variablesGlobales.warriorP2);
                        break;
                    case 3:
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("Validation").gameObject.SetActive(true);
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(true);
						VoixGodselect(variablesGlobales.warriorP3);
                        break;
                    case 4:
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("Validation").gameObject.SetActive(true);
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(true);
						VoixGodselect(variablesGlobales.warriorP4);
                        break;
                }
                checkIfAllReady();
            }
        }
		//Fx
		GameObject.Find("World").GetComponent<soundControl>().JouerFx ("fxValider");
    }
    private void checkIfAllReady()
    {
        bool allValid = true;
        foreach (PlayerControllerIdentifier unController in _lesControllers)
        {
            if (!unController.heroChoose)
            {
                allValid = false;
            }
        }
        foreach (PlayerControllerIdentifier unController in _lesControllers)
        {
            if (unController.heroChoose)
            {
                //Panel Wait :
                switch (unController.idSlotPlayer)
                {
                    case 1:
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("Validation").gameObject.SetActive(true);
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(true);
                        break;
                    case 2:
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("Validation").gameObject.SetActive(true);
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(true);
                        break;
                    case 3:
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("Validation").gameObject.SetActive(true);
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(true);
                        break;
                    case 4:
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("Validation").gameObject.SetActive(true);
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(true);
                        break;
                }
                if (allValid && _lesControllers.Count>1)
                {
                    if (unController.id>4)//Clavier :
                    {
                        string txtSubmitCmd = "";
                        switch (unController.id)
                        {
                            case 5:
                                txtSubmitCmd = "Z";
                                break;
                            case 6:
                                txtSubmitCmd = "->";
                                break;
                            case 7:
                                txtSubmitCmd = "8";
                                break;
                            case 8:
                                txtSubmitCmd = "O";
                                break;
                        }
                        switch (unController.idSlotPlayer)
                        {
                            case 1:
                                playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(false);
                                playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("PanelFightJoy").gameObject.SetActive(false);
                                playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").gameObject.SetActive(true);
                                playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").FindChild("txtTouche").GetComponent<Text>().text = txtSubmitCmd;
                                if (unController.id == 6)
                                {
                                    playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").FindChild("txtTouche").rotation = Quaternion.Euler(0, 0, 90);
                                }
                                else
                                {
                                    playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").FindChild("txtTouche").rotation = new Quaternion(0, 0, 0, 0);
                                }
                                break;
                            case 2:
                                playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(false);
                                playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("PanelFightJoy").gameObject.SetActive(false);
                                playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").gameObject.SetActive(true);
                                playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").FindChild("txtTouche").GetComponent<Text>().text = txtSubmitCmd;
                                if (unController.id == 6)
                                {
                                    playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").FindChild("txtTouche").rotation = Quaternion.Euler(0, 0, 90);
                                }
                                else
                                {
                                    playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").FindChild("txtTouche").rotation = new Quaternion(0, 0, 0, 0);
                                }
                                break;
                            case 3:
                                playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(false);
                                playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("PanelFightJoy").gameObject.SetActive(false);
                                playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").gameObject.SetActive(true);
                                playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").FindChild("txtTouche").GetComponent<Text>().text = txtSubmitCmd;
                                if (unController.id == 6)
                                {
                                    playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").FindChild("txtTouche").rotation = Quaternion.Euler(0, 0, 90);
                                }
                                else
                                {
                                    playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").FindChild("txtTouche").rotation = new Quaternion(0, 0, 0, 0);
                                }
                                break;
                            case 4:
                                playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(false);
                                playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("PanelFightJoy").gameObject.SetActive(false);
                                playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").gameObject.SetActive(true);
                                playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").FindChild("txtTouche").GetComponent<Text>().text = txtSubmitCmd;
                                if (unController.id == 6)
                                {
                                    playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").FindChild("txtTouche").rotation = Quaternion.Euler(0,0,90);
                                }
                                else
                                {
                                    playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").FindChild("txtTouche").rotation = new Quaternion(0, 0, 0, 0);
                                }
                                break;
                        }
                    }
                    else //Manette
                    {
                        switch (unController.idSlotPlayer)
                        {
                            case 1:
                                playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(false);
                                playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("PanelFightJoy").gameObject.SetActive(true);
                                playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").gameObject.SetActive(false);
                                break;
                            case 2:
                                playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(false);
                                playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("PanelFightJoy").gameObject.SetActive(true);
                                playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").gameObject.SetActive(false);
                                break;
                            case 3:
                                playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(false);
                                playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("PanelFightJoy").gameObject.SetActive(true);
                                playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").gameObject.SetActive(false);
                                break;
                            case 4:
                                playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(false);
                                playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("PanelFightJoy").gameObject.SetActive(true);
                                playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").gameObject.SetActive(false);
                                break;
                        }
                    }
                }
                else
                {
                    switch (unController.idSlotPlayer)
                    {
                        case 1:
                            playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("PanelFightJoy").gameObject.SetActive(false);
                            playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").gameObject.SetActive(false);
                            break;
                        case 2:
                            playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("PanelFightJoy").gameObject.SetActive(false);
                            playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").gameObject.SetActive(false);
                            break;
                        case 3:
                            playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("PanelFightJoy").gameObject.SetActive(false);
                            playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").gameObject.SetActive(false);
                            break;
                        case 4:
                            playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("PanelFightJoy").gameObject.SetActive(false);
                            playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").gameObject.SetActive(false);
                            break;
                    }
                }
            }
            else
            {
                switch (unController.idSlotPlayer)
                {
                    case 1:
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("Validation").gameObject.SetActive(false);
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(false);
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("PanelFightJoy").gameObject.SetActive(false);
                        playerPanel1.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").gameObject.SetActive(false);
                        break;
                    case 2:
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("Validation").gameObject.SetActive(false);
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(false);
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("PanelFightJoy").gameObject.SetActive(false);
                        playerPanel2.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").gameObject.SetActive(false);
                        break;
                    case 3:
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("Validation").gameObject.SetActive(false);
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(false);
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("PanelFightJoy").gameObject.SetActive(false);
                        playerPanel3.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").gameObject.SetActive(false);
                        break;
                    case 4:
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("Validation").gameObject.SetActive(false);
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("PanelWait").gameObject.SetActive(false);
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("PanelFightJoy").gameObject.SetActive(false);
                        playerPanel4.transform.FindChild("PanelHaveJoin").FindChild("PanelFightKey").gameObject.SetActive(false);
                        break;
                }
            }
        }

    }
    private int getIdSlotWithIdController(int unId)
    {
        int slot = -1;
        foreach (PlayerControllerIdentifier unController in _lesControllers)
        {
            if (unController.id == unId)
            {
                slot = unController.idSlotPlayer;
            }
        }
        return slot;
    }
    public void changerMicrophone(bool microActiver)
    {
        microActif = microActiver;
    }
}
public class PlayerControllerIdentifier
{
    private int _id;
    private int _idSlotPlayer;
    private bool _heroChoose;

    public PlayerControllerIdentifier(int unId, int unSlotPlayer)
    {
        this._id = unId;
        this._idSlotPlayer = unSlotPlayer;
        this._heroChoose = false;
    }
    public int id
    {
        get { return this._id; }
    }
    public int idSlotPlayer
    {
        get { return this._idSlotPlayer; }
    }
    public bool heroChoose
    {
        get { return this._heroChoose; }
        set { this._heroChoose = value; }
    }
}


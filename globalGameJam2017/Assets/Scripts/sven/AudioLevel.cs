﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

// ---------------------------------------------------
///<summary>
/// Class
///</summary>
public class AudioLevel : ScriptableObject {

#region Definition
#endregion

#region Attributes
    public AudioClip clip;
    public int bmp;
    public float[] mult;

#endregion


    //------------------------------------------------------
    // Add Menu Item
    //------------------------------------------------------
#if UNITY_EDITOR
    [MenuItem("Assets/Create/Audio Level")]
    public static void CreateMyAsset()
    {

        AudioLevel audioLevel = ScriptableObject.CreateInstance<AudioLevel>();
        audioLevel.mult = new float[16];

        AssetDatabase.CreateAsset(audioLevel, "Assets/Resources/AudioLevels/NewLevel.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = audioLevel;
    }
#endif
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AudioAnalizer))]
public class AudioAnalizerEditor : Editor {

    private AudioAnalizer _aa;
    private SerializedProperty _spAudioLevel;
    private SerializedProperty _spMicrophone;
    private SerializedProperty _spMix;
    private SerializedProperty _spPlayOnStart;
    private SerializedProperty _spAnalyzeMode;
    private SerializedProperty _spChannel;
    private SerializedProperty _spBumpers;
    private SerializedProperty _spMult;
    private SerializedProperty _spMicrophoneMult;

    //------------------------------------------------
    /// <summary>
    /// On Enable
    /// </summary>
	void OnEnable()
    {
        this._aa = (AudioAnalizer)target;
        this._spAudioLevel = this.serializedObject.FindProperty("audioLevel");
        this._spMicrophone = this.serializedObject.FindProperty("microphone");
        this._spMix = this.serializedObject.FindProperty("mix");
        this._spPlayOnStart = this.serializedObject.FindProperty("playOnStart");
        this._spAnalyzeMode = this.serializedObject.FindProperty("analyzeMode");
        this._spChannel = this.serializedObject.FindProperty("channel");
        this._spBumpers = this.serializedObject.FindProperty("bumpers");
        this._spMult = this.serializedObject.FindProperty("_mult");
        this._spMicrophoneMult = this.serializedObject.FindProperty("_microphoneMult");
    }

    //------------------------------------------------
    /// <summary>
    /// On Inspector GUI
    /// </summary>
	override public void OnInspectorGUI()
    {
        //Update serialized object
        this.serializedObject.Update();

        //Default Mult
        if (this._spMult.arraySize == 0)
        {
            float[] mult = AudioAnalizer.GetDefaultMult();
            this._spMult.arraySize = mult.Length;
            for (int i=0; i<mult.Length; i++)
            {
                this._spMult.GetArrayElementAtIndex(i).floatValue = mult[i];
            }
            this.serializedObject.ApplyModifiedProperties();
        }
        if (this._spMicrophoneMult.arraySize == 0)
        {
            float[] mult = AudioAnalizer.GetDefaultMult();
            this._spMicrophoneMult.arraySize = mult.Length;
            for (int i = 0; i < mult.Length; i++)
            {
                this._spMicrophoneMult.GetArrayElementAtIndex(i).floatValue = mult[i];
            }
            this.serializedObject.ApplyModifiedProperties();
        }

        //Draw inspector
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(this._spAudioLevel);

        //Microphone Devices
        List<string> devices = new List<string>();
        devices.Add("No");
        int selectedMic = 0;
        for(int d=0; d<Microphone.devices.Length; d++)
        {
            devices.Add(Microphone.devices[d]);
            if (this._spMicrophone.stringValue == Microphone.devices[d]) selectedMic = d + 1;
        }

        int newMic = EditorGUILayout.Popup("Mic Input", selectedMic, devices.ToArray(), EditorStyles.popup);
        if(newMic != selectedMic)
        {
            string mic = devices[newMic];
            if (mic == "No") mic = "";
            this._spMicrophone.stringValue = mic;
            GUI.changed = true;
        }
        EditorGUILayout.PropertyField(this._spMix, new GUIContent("Mix Mic and Song"));
        EditorGUILayout.PropertyField(this._spPlayOnStart);
        EditorGUILayout.PropertyField(this._spAnalyzeMode);
        EditorGUILayout.PropertyField(this._spChannel);
        EditorGUILayout.PropertyField(this._spBumpers,true);

        //AudioLevel MULT
        if (string.IsNullOrEmpty(this._spMicrophone.stringValue))
        {
            AudioLevel level = this._aa.audioLevel;
            EditorGUILayout.PropertyField(this._spMult, true);
            if (level && GUILayout.Button("Save AudioLevel Mult"))
            {
                level.mult = new float[16];
                this._spMult.arraySize = 16;
                for (int m = 0; m < this._spMult.arraySize; m++)
                {
                    level.mult[m] = this._spMult.GetArrayElementAtIndex(m).floatValue;
                }
                EditorUtility.SetDirty(level);
                AssetDatabase.SaveAssets();
            }
        }
        //Microphone MULT
        else EditorGUILayout.PropertyField(this._spMicrophoneMult, true);

        //Save Changes
        EditorGUI.EndChangeCheck();
        if (GUI.changed) this.serializedObject.ApplyModifiedProperties();
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

// ---------------------------------------------------
///<summary>
/// Class
///</summary>
public class MusicSelector : MonoBehaviour
{

    #region Definition
    #endregion

    #region Attributes
    public AudioAnalizer audioAnalizer;

    private List<AudioClip> _clips = new List<AudioClip>();
    private FileInfo[] _soundFiles;
    private Dropdown _dropdown;

    private List<string> _validExtensions = new List<string> { ".ogg" }; // Don't forget the "." i.e. "ogg" won't work - cause Path.GetExtension(filePath) will return .ext, not just ext.
    private string _absolutePath = "./"; // relative path to where the app is running - change this to "./music" in your case
    private string _musicFolder = "Music/";
    #endregion

    // ---------------------------------------------------
    ///<summary>
    /// Start
    ///</summary>
    void Start()
    {
        this._dropdown = this.GetComponent<Dropdown>();
#if UNITY_EDITOR
        this._absolutePath = "Assets/";
#endif
        this.ListSounds();
    }

    // ---------------------------------------------------
    ///<summary>
    /// ListSounds
    ///</summary>
    void ListSounds()
    {
        this._clips.Clear();
        // get all valid files
        var info = new DirectoryInfo(this._absolutePath + this._musicFolder);
        this._soundFiles = info.GetFiles()
            .Where(f => IsValidFileType(f.Name))
            .ToArray();

        // and load them
        foreach (var s in this._soundFiles)
            StartCoroutine(this.LoadFile(s.FullName));
    }

    // ---------------------------------------------------
    ///<summary>
    /// IsValidFileType
    ///</summary>
    bool IsValidFileType(string fileName)
    {
        return _validExtensions.Contains(Path.GetExtension(fileName));
    }

    // ---------------------------------------------------
    ///<summary>
    /// IsValidFileType
    ///</summary>
    IEnumerator LoadFile(string path)
    {
        WWW www = new WWW("file://" + path);

        AudioClip clip = www.GetAudioClip(false);
        while (clip.loadState != AudioDataLoadState.Loaded)
        {
            if (clip.loadState == AudioDataLoadState.Failed)
            {
                Debug.Log("Failed to load " + path);
                break;
            }
            yield return www;
        }
        if(clip.loadState == AudioDataLoadState.Loaded)
        {
            clip.name = Path.GetFileName(path);
            this._clips.Add(clip);
            this.UpdateDropDown();
        }
    }

    // ---------------------------------------------------
    ///<summary>
    /// Update Drop Down
    ///</summary>
    private void UpdateDropDown()
    {
        List<Dropdown.OptionData> options = new List<Dropdown.OptionData>();
        options.Add(new Dropdown.OptionData("Select Music..."));
        for (int c = 0; c < this._clips.Count; c++)
        {
            options.Add(new Dropdown.OptionData(this._clips[c].name));
        }
        this._dropdown.ClearOptions();
        this._dropdown.options = options;
    }

    // ---------------------------------------------------
    ///<summary>
    /// Update Drop Down
    ///</summary>
    public void ChangeMusic(int index)
    {
        if (!this.audioAnalizer) return;
        this.audioAnalizer.GetComponent<AudioSource>().Stop();

        float[] mult = new float[]
        {
            4,
            8,
            15,
            20,
            20,
            25,
            25,
            25,
            30,
            30,
            30,
            35,
            35,
            35,
            40,
            40
        };
        this.audioAnalizer.Play(this._clips[this._dropdown.value-1], 0, mult);
    }

}
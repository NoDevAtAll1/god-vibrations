﻿using UnityEngine;
using System.Collections;

// ---------------------------------------------------
///<summary>
/// Item
///</summary>
public class Item : MonoBehaviour
{

    #region Definition
    public enum ItemType
    {
        BUBBLE = 0,
        INVINCIBILITY = 1,
        INVERSION = 2,
        FREEZE = 3,
        LIFE = 4
    }
    #endregion

    #region Attributes
    public ItemsManager manager;

    private ItemType _type;
    private SpriteRenderer _spriteRenderer;
    new private Collider2D _collider;
    private bool _catched = false;
    #endregion
    
    // ---------------------------------------------------
    ///<summary>
    /// Awake
    ///</summary>
    private void Awake()
    {
        this._spriteRenderer = this.GetComponent<SpriteRenderer>();
        this._collider = this.GetComponent<Collider2D>();
    }

    // ---------------------------------------------------
    ///<summary>
    /// Set Type
    ///</summary>
    public void SetType(ItemType type)
    {
        this._type = type;
        this._spriteRenderer.sprite = this.manager.itemSprites[(int)type];
    }

    // ---------------------------------------------------
    ///<summary>
    /// OnTriggerEnter2D
    ///</summary>
    void OnTriggerEnter2D(Collider2D obj)
    {
        //Check if Player
        if(obj.gameObject.tag != "Player") return;

        //Contournement Bug Manager 
        if(!this.manager) return;

        //Catched
        if (this._catched) return;
        this._catched = true;

        //Action
        PlayerLife player = obj.GetComponent<PlayerLife>();
        switch (this._type)
        {
            //--------------------------------------
            //  Apply to player
            case ItemType.BUBBLE:
            case ItemType.INVINCIBILITY:
            case ItemType.LIFE:
            
                player.SetBonus(this._type);
                break;
            //--------------------------------------
            //  Apply to other players
            case ItemType.FREEZE:
            case ItemType.INVERSION:
                GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
                for(int p=0; p<players.Length; p++)
                {
                    PlayerLife other = players[p].GetComponent<PlayerLife>();
                    if (!other || other == player) continue;
                    other.SetBonus(this._type);
                }
                break;
        }
		//On affiche le feedback de l'item
		if(variablesGlobales.modeBardBall == false) GameObject.Find("World").GetComponent<GameManager>().FeedbackItems(this._type);
		else GameObject.Find("World").GetComponent<GameManagerBardeBall>().FeedbackItems(this._type);

		//Soutien du public
		int numPlayer = obj.gameObject.transform.GetComponent<PlayerControllerMartin>().idJoueur;
		if(variablesGlobales.modeBardBall == true) GameObject.Find("World").GetComponent<GameManagerBardeBall>().RandomCriDuPublic(numPlayer);
		else GameObject.Find("World").GetComponent<GameManager>().RandomCriDuPublic(numPlayer);

        //Add new item and destroy self
        this.manager.AddItem();
        GameObject.Destroy(this.gameObject);
    }
}
﻿using UnityEngine;
using System.Collections;


// ---------------------------------------------------
///<summary>
/// Class
///</summary>
public class ItemsManager : MonoBehaviour {

    #region Definition
    #endregion

    #region Attributes

    public GameObject[] spawners;
    public GameObject prefab;

    public Sprite[] itemSprites;

    private float _spawnMinTime = 6f;
    private float _spawnMaxTime = 12f;

    #endregion

    // ---------------------------------------------------
    ///<summary>
    /// Start
    ///</summary>
    void Start () {
        this.AddItem();
    }
		
	// ---------------------------------------------------
	///<summary>
	/// AddItem
	///</summary>
	public void AddItem () {
        this.StartCoroutine(this.SpawnItem());
	}

    // ---------------------------------------------------
    ///<summary>
    /// Spawn Item
    ///</summary>
    public IEnumerator SpawnItem()
    {
        //Time Before Spawning
        yield return new WaitForSeconds(Random.Range(this._spawnMinTime, this._spawnMaxTime));

        //Random Item Type
        int id = Random.Range(0, System.Enum.GetNames(typeof(Item.ItemType)).Length - 1);
		if(variablesGlobales.etatJeu == "playBardeball" && id == 1) id = 3;
        Item.ItemType type = (Item.ItemType)id;

        // // // // // // // //
        //type = Item.ItemType.BUBBLE;
        // // // // // // // //

        //Search a free spawner
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        GameObject spawner = null;
        while(!spawner)
        {
            int spawnerId = Random.Range(0, this.spawners.Length - 1);
            spawner = this.spawners[spawnerId];
            bool occupied = false;
            for(int p=0; p<players.Length; p++)
            {
                Collider2D[] colliders = players[p].GetComponents<Collider2D>();
                for(int c=0; c<colliders.Length; c++)
                {
                    occupied = colliders[c].OverlapPoint(new Vector2(spawner.transform.position.x, spawner.transform.position.y));
                    if (occupied) break;
                }
                if (occupied) break;
            }
            if (occupied)
            {
                spawner = null;
                yield return null;
                continue;
            }
            else break;
        }

        //Instantiate new Item
        GameObject newGo = Instantiate(this.prefab, spawner.transform) as GameObject;
        newGo.name = "item";
        newGo.transform.position = spawner.transform.position;
        Item newItem = newGo.GetComponent<Item>();
        newItem.manager = this;
        newItem.SetType(type);
    }
}
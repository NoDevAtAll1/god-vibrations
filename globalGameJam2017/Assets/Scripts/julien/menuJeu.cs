﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class menuJeu : MonoBehaviour {

	//Variables
	public GameObject EcranPresent;
	public GameObject EcranIntro;

	private bool resetBouton1;

	//Use this for initialization
	void Start ()
	{
		EcranPresent.SetActive (true);
		EcranIntro.SetActive (false);
	}
	
	//Update is called once per frame
	void Update ()
	{
		if(variablesGlobales.etatJeu == "chooseIntro")
		{
			if(Input.GetAxis("Submit") == 1 || Input.GetAxis("Submit2") == 1 || Input.GetAxis("Submit3") == 1 || Input.GetAxis("Submit4") == 1 || Input.GetAxis("Submit5") == 1 || Input.GetAxis("Submit6") == 1 || Input.GetAxis("Submit7") == 1|| Input.GetAxis("Submit8") == 1 && resetBouton1)
			{
				resetBouton1 = false;
				AfficherEcranTitre();
			}
			else if(Input.GetAxis("Submit") == 0 || Input.GetAxis("Submit2") == 0 || Input.GetAxis("Submit3") == 0 || Input.GetAxis("Submit4") ==0 || Input.GetAxis("Submit5") == 0 || Input.GetAxis("Submit6") == 0 || Input.GetAxis("Submit7") == 0|| Input.GetAxis("Submit8") == 0 && !resetBouton1)
			{
				resetBouton1 = true;
			}
		}
	}

	//Suite vers intro
	public void AfficherIntro() {
		variablesGlobales.etatJeu = "chooseIntro";
		EcranIntro.SetActive(true);
		EcranPresent.SetActive(false);
		//Musique
		GameObject.Find("Scripts").GetComponent<soundControl>().JouerMusique("musiqueIntro");
	}

	//Suite vers ecran-titre
	public void AfficherEcranTitre()
	{
		variablesGlobales.etatJeu = "chooseMenu";
		SceneManager.LoadScene("SceneMenuV2");
	}
}

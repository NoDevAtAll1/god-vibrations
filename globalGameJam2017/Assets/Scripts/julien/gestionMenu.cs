﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gestionMenu : MonoBehaviour {

	//Slider
	public GameObject sliderFx;

	// Use this for initialization
	void Start()
	{
		variablesGlobales.modeBardBall = false;
		variablesGlobales.scoreP1 = 0;
		variablesGlobales.scoreP2 = 0;
		variablesGlobales.scoreP3 = 0;
		variablesGlobales.scoreP4 = 0;
	}
	
	public void InitModeBardBall()
	{
		variablesGlobales.modeBardBall = true;
		SceneManager.LoadScene("SceneMenuV2Selection");
	}

	public void GestionVolumeFx()
	{
		variablesGlobales.volumeFx = sliderFx.GetComponent<Slider>().value;
		GameObject.Find("Main Camera").transform.FindChild("Fx").GetComponent<AudioSource>().volume = variablesGlobales.volumeFx;
	}
}

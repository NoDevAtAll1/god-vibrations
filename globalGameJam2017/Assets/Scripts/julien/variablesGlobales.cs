﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class variablesGlobales : MonoBehaviour
{
    public static List<PlayerControllerIdentifier> lesControllersActifs = new List<PlayerControllerIdentifier>();
	public static string etatJeu = "init";
	public static bool modeBardBall = false;
	public static int nbrPlayers = 4;
	public static int numStage = 1;
	public static int maxStage = 7;
    public static bool microphoneActiver = false;

	public static int nbrWarrior = 4;
	public static int warriorP1 = 1;
	public static int warriorP2 = 2;
	public static int warriorP3 = 3;
	public static int warriorP4 = 4;
    public static string player1ControllerAxis = "null";
    public static string player2ControllerAxis = "null";
    public static string player3ControllerAxis = "null";
    public static string player4ControllerAxis = "null";

    public static int scoreP1 = 0;
	public static int scoreP2 = 0;
	public static int scoreP3 = 0;
	public static int scoreP4 = 0;

	public static int vieP1 = 0;
	public static int vieP2 = 1;
	public static int vieP3 = 0;
	public static int vieP4 = 0;

	//Barde ball
	public static int goalP1 = 0;
	public static int goalP2 = 0;
	public static int goalP3 = 0;
	public static int goalP4 = 0;
	public static int goalsToWin = 5;

	public static string[] tabTextesWarriors = {
		"Anubis",
		"Huitzilopochtli",
		"Jakuta",
		"Tyr"
	};

	public static string[,] tabTextesStages = {
		{"Egypt, Duat","Sand, sun, faithful people, lazy pharaohs : a land of prosperity... For these very reasons, Anubis prefered calm and shady landscape of dead's realm."},
		{"Aztec, Mitlan","Traveling through this world takes normally four years. Four years accompanied by darkness. And big masks."},
		{"Fjord, Helheim","If you didn't died as a true warrior, that's THE place to be. Cold, wet, and foggy, it's your deserved dream destination as a loser."},
		{"Africa, Kalunga","A whole country dived in the mourning of a narcissist king who thought that tears will bring her queen back. Retard. Huh, romantic I mean."},
		{"Avalon, Bard's Forest","Your favorite story teller was born and raised here, surrounded by cute (boring) animals. Really peaceful, unless the bard is singing or you decided to take your nap below the comforting shade of a carnivorous plant."},
		{"???, The Sacrificial Temple","A mysterious location lost in a threatening forest. Some desperate (chicken) calls, tearing the night, are reverberating within walls of this imposing temple."},
		{"Random stage","Let fate choose your fighting place!"}
	};

	public static string[] tabMusics = {
		"♫ Anubeats (110 BPM) ♫",
		"♫ Aztec Merengue (150 BPM) ♫",
		"♫ Tyr For Fear (170 BPM) ♫",
		"♫ Jakuta Trap (130 BPM) ♫",
		"♫ Forest Jump (Horn-chestral Mix) (155 BPM) ♫",
		"♫ The Sacrificial Temple (150 BPM) ♫",
		"♫ Kami Dance (150 BPM) ♫"
	};

	public static float volumeFx = 0.8f;
}

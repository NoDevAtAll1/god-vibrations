﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class soundControl : MonoBehaviour {

	//Variables
	public AudioClip fxBtn1;
	public AudioClip fxBtn2;
	public AudioClip fxMouv;
	public AudioClip fxValider;
	public AudioClip fxResultat;

	public AudioClip fxNomAnubis;
	public AudioClip fxNomJakuta;
	public AudioClip fxNomHuitzi;
	public AudioClip fxNomTyr;

	public AudioClip fxStageAfrica;
	public AudioClip fxStageAztec;
	public AudioClip fxStageNordic;
	public AudioClip fxStageEgypt;
	public AudioClip fxStageForest;
	public AudioClip fxStageChickenRune;

	public AudioClip musiqueIntro;
	public AudioClip musiqueMenu;

	//Fonctions
	public void JouerFx(string idSon)
	{
		switch(idSon)
		{
			case "fxBtn1" :
				GameObject.Find("Fx").GetComponent<AudioSource>().PlayOneShot(fxBtn1);
				break;
			case "fxBtn2" :
				GameObject.Find("Fx").GetComponent<AudioSource>().PlayOneShot(fxBtn2);
				break;
			case "fxMouv" :
				GameObject.Find("Fx").GetComponent<AudioSource>().PlayOneShot(fxMouv);
				break;
			case "fxValider":
				GameObject.Find ("Fx").GetComponent<AudioSource> ().PlayOneShot (fxValider);
				break;
			case "fxResultat":
				GameObject.Find ("Fx").GetComponent<AudioSource> ().PlayOneShot (fxResultat);
				break;
			case "fxNomAnubis":
				GameObject.Find ("Fx").GetComponent<AudioSource> ().PlayOneShot (fxNomAnubis);
				break;
			case "fxNomJakuta":
				GameObject.Find ("Fx").GetComponent<AudioSource> ().PlayOneShot (fxNomJakuta);
				break;
			case "fxNomHuitzi":
				GameObject.Find ("Fx").GetComponent<AudioSource> ().PlayOneShot (fxNomHuitzi);
				break;
			case "fxNomTyr":
				GameObject.Find ("Fx").GetComponent<AudioSource> ().PlayOneShot (fxNomTyr);
				break;
			case "fxStageAfrica":
				GameObject.Find ("Fx").GetComponent<AudioSource> ().PlayOneShot (fxStageAfrica);
				break;
			case "fxStageAztec":
				GameObject.Find ("Fx").GetComponent<AudioSource> ().PlayOneShot (fxStageAztec);
				break;
			case "fxStageNordic":
				GameObject.Find ("Fx").GetComponent<AudioSource> ().PlayOneShot (fxStageNordic);
				break;
			case "fxStageEgypt":
				GameObject.Find ("Fx").GetComponent<AudioSource> ().PlayOneShot (fxStageEgypt);
				break;
			case "fxStageForest":
				GameObject.Find ("Fx").GetComponent<AudioSource> ().PlayOneShot (fxStageForest);
				break;
			case "fxStageChickenRune":
				GameObject.Find ("Fx").GetComponent<AudioSource> ().PlayOneShot (fxStageChickenRune);
				break;
		}
	}

	public void JouerMusique(string idMusique)
	{
		GameObject.Find("Music").GetComponent<AudioSource>().Stop();
		switch(idMusique)
		{
			case "musiqueIntro" :
				GameObject.Find("Music").GetComponent<AudioSource>().clip = musiqueIntro;
				break;	
			case "musiqueMenu" :
					GameObject.Find("Music").GetComponent<AudioSource>().clip = musiqueMenu;
					break;
		}
		GameObject.Find("Music").GetComponent<AudioSource>().Play();
	}

	public void StopperMusique()
	{
		GameObject.Find("Music").GetComponent<AudioSource>().Stop();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class GameManagerBardeBall : MonoBehaviour {

	public GameObject ParentJoueur;
	public GameObject God1;
	public GameObject God2;
	public GameObject God3;
	public GameObject God4;
	public GameObject LabelInfo1;
	public GameObject LabelInfo2;
	public GameObject LabelInfo3;
	public GameObject LabelMusique;
	public GameObject MiniBut;
	public GameObject Fond;
	public AudioClip FxStart;
	public AudioClip FxResultat;
	public AudioClip FxItemActivated;
	public AudioClip FxGoal;
	public AudioClip FxPublic;

	//Voix
	public AudioClip FxGoal1;
	public AudioClip FxGoal2;
	public AudioClip FxGoal3;
	public AudioClip FxGoal4;
	public AudioClip FxGoal5;
	public AudioClip FxGoal6;
	public AudioClip FxItemShield;
	public AudioClip FxItemInversion;
	public AudioClip FxItemBubble;
	public AudioClip FxItemFreeze;
	public AudioClip FxPublicJakuta;
	public AudioClip FxPublicAnubis;
	public AudioClip FxPublicTyr;
	public AudioClip FxPublicHuitzi;

	public AudioClip FxAfrica;
	public AudioClip FxAzTech;
	public AudioClip FxBarde;
	public AudioClip FxEgypt;
	public AudioClip FxNordic;
	public AudioClip FxChikenRune;
	public AudioClip FxJapan;

	public AudioSource AudioS;
	public AudioAnalizer AudioA;
	public GameObject PanelResultat;
	public GameObject PanelPause;
	public GameObject Score;

	private GameObject P1;
	private GameObject P2;
	private GameObject P3;
	private GameObject P4;
	private int nbrJoueurs;
	private int nbrJoueurValidatePausePlay;
	private int nbrJoueurValidatePauseQuit;

	// Use this for initialization
	void Start ()
	{
		variablesGlobales.etatJeu = "bardball";
		PanelResultat.SetActive(false);
		LabelMusique.SetActive(false);
		nbrJoueurs = variablesGlobales.nbrPlayers;
		//Goal
		variablesGlobales.goalP1 = 0;
		variablesGlobales.goalP2 = 0;
		variablesGlobales.goalP3 = 0;
		variablesGlobales.goalP4 = 0;
		//Les joueurs
		//P1
		if (variablesGlobales.warriorP1 == 1)
			P1 = Instantiate (God1, ParentJoueur.transform) as GameObject;
		else if (variablesGlobales.warriorP1 == 2)
			P1 = Instantiate (God2, ParentJoueur.transform) as GameObject;
		else if (variablesGlobales.warriorP1 == 3)
			P1 = Instantiate (God3, ParentJoueur.transform) as GameObject;
		else if (variablesGlobales.warriorP1 == 4)
			P1 = Instantiate (God4, ParentJoueur.transform) as GameObject;
		P1.transform.GetComponent<PlayerControllerMartin> ().SetIdJoueur (1);
		//Vie
		P1.transform.FindChild("Life").FindChild("Vie1 (1)").GetComponent<SpriteRenderer>().color = Color.red;
		P1.transform.FindChild("Life").FindChild("Vie1").gameObject.SetActive(false);
		P1.transform.FindChild("Life").FindChild("Vie1 (2)").gameObject.SetActive(false);
		//Volume audio
		P1.GetComponent<AudioSource>().volume = variablesGlobales.volumeFx;
		//P2
		if (variablesGlobales.warriorP2 == 1)
			P2 = Instantiate (God1, ParentJoueur.transform) as GameObject;
		else if (variablesGlobales.warriorP2 == 2)
				P2 = Instantiate (God2, ParentJoueur.transform) as GameObject;
		else if (variablesGlobales.warriorP2 == 3)
				P2 = Instantiate (God3, ParentJoueur.transform) as GameObject;
		else if (variablesGlobales.warriorP2 == 4)
				P2 = Instantiate (God4, ParentJoueur.transform) as GameObject;
		P2.transform.GetComponent<PlayerControllerMartin> ().SetIdJoueur (2);
		//Vie
		P2.transform.FindChild("Life").FindChild("Vie1 (1)").GetComponent<SpriteRenderer>().color = Color.blue;
		P2.transform.FindChild("Life").FindChild("Vie1").gameObject.SetActive(false);
		P2.transform.FindChild("Life").FindChild("Vie1 (2)").gameObject.SetActive(false);
		//Volume audio
		P2.GetComponent<AudioSource>().volume = variablesGlobales.volumeFx;
		//P3
		if (variablesGlobales.nbrPlayers > 2) {
			if (variablesGlobales.warriorP3 == 1)
						P3 = Instantiate (God1, ParentJoueur.transform) as GameObject;
			else if (variablesGlobales.warriorP3 == 2)
						P3 = Instantiate (God2, ParentJoueur.transform) as GameObject;
			else if (variablesGlobales.warriorP3 == 3)
						P3 = Instantiate (God3, ParentJoueur.transform) as GameObject;
			else if (variablesGlobales.warriorP3 == 4)
						P3 = Instantiate (God4, ParentJoueur.transform) as GameObject;
			P3.transform.GetComponent<PlayerControllerMartin> ().SetIdJoueur (3);
			//Vie
			P3.transform.FindChild("Life").FindChild("Vie1 (1)").GetComponent<SpriteRenderer>().color = new Color(0,0.8f,0.2f);
			P3.transform.FindChild("Life").FindChild("Vie1").gameObject.SetActive(false);
			P3.transform.FindChild("Life").FindChild("Vie1 (2)").gameObject.SetActive(false);
			//Volume audio
			P3.GetComponent<AudioSource>().volume = variablesGlobales.volumeFx;
		}
		else {
			Score.transform.FindChild("P3").gameObject.SetActive(false);
		}
		//P4
		if (variablesGlobales.nbrPlayers > 3) {
			if (variablesGlobales.warriorP4 == 1)
						P4 = Instantiate (God1, ParentJoueur.transform) as GameObject;
			else if (variablesGlobales.warriorP4 == 2)
						P4 = Instantiate (God2, ParentJoueur.transform) as GameObject;
			else if (variablesGlobales.warriorP4 == 3)
						P4 = Instantiate (God3, ParentJoueur.transform) as GameObject;
			else if (variablesGlobales.warriorP4 == 4)
						P4 = Instantiate (God4, ParentJoueur.transform) as GameObject;
			P4.transform.GetComponent<PlayerControllerMartin> ().SetIdJoueur (4);
			//Vie
			P4.transform.FindChild("Life").FindChild("Vie1 (1)").GetComponent<SpriteRenderer>().color = new Color(0.8f,0,0.9f);
			P4.transform.FindChild("Life").FindChild("Vie1").gameObject.SetActive(false);
			P4.transform.FindChild("Life").FindChild("Vie1 (2)").gameObject.SetActive(false);
			//Volume audio
			P4.GetComponent<AudioSource>().volume = variablesGlobales.volumeFx;
		}
		else {
			Score.transform.FindChild("P4").gameObject.SetActive(false);
		}
		//Mode nuit
		int modeNuit = (int)Random.Range(1, 5.5f);
		if(modeNuit == 5)
		{
			Fond.GetComponent<SpriteRenderer>().color = new Color(0.33f, 0.45f, 1f);
			int nbrChild = GameObject.Find("Bumpers").transform.childCount;
			for(int c = 0; c < nbrChild; c++)
			{
				GameObject.Find("Bumpers").transform.GetChild(c).GetComponent<SpriteRenderer>().color = new Color(0.3f, 0.35f, 0.6f);
			}
		}
		//Mode chicken ball
		int chickenBall = (int)Random.Range(1, 10.5f);
		if(chickenBall == 10)
		{
			GameObject.Find("PlayerBall").transform.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("chickenball") as Sprite;
		}
		//Volume audio
		AudioS.GetComponent<AudioSource>().volume = variablesGlobales.volumeFx;
		//Start
		StartCoroutine(GestionDepart());
	}

	IEnumerator GestionDepart()
	{
		LabelInfo1.GetComponent<Text> ().text = "Ready ?";
		//Fx
		AudioS.GetComponent<AudioSource>().PlayOneShot(FxStart);
		yield return new WaitForSeconds(1.2f);
		variablesGlobales.etatJeu = "playBardeball";
		LabelInfo1.GetComponent<Text> ().text = "3";
		yield return new WaitForSeconds(1);
		LabelInfo1.GetComponent<Text> ().text = "2";
		yield return new WaitForSeconds(0.7f);
		LabelInfo1.GetComponent<Text> ().text = "1";
		yield return new WaitForSeconds(1f);
		LabelInfo1.GetComponent<Text> ().text = "";
		//Choix de la musique
		int numMusique = (int)Random.Range(1, 7.5f);
		switch(numMusique)
		{
			case 4:
				AudioA.GetComponent<AudioAnalizer>().audioLevel = Resources.Load<AudioLevel>("AudioLevels/Africa");
				AudioA.GetComponent<AudioSource>().clip = FxAfrica;
				break;
			case 2:
				AudioA.GetComponent<AudioAnalizer>().audioLevel = Resources.Load<AudioLevel>("AudioLevels/AzTech");
				AudioA.GetComponent<AudioSource>().clip = FxAzTech;
				break;
			case 5:
				AudioA.GetComponent<AudioAnalizer>().audioLevel = Resources.Load<AudioLevel>("AudioLevels/Barde");
				AudioA.GetComponent<AudioSource>().clip = FxBarde;
				break;
			case 1:
				AudioA.GetComponent<AudioAnalizer>().audioLevel = Resources.Load<AudioLevel>("AudioLevels/Egypt");
				AudioA.GetComponent<AudioSource>().clip = FxEgypt;
				break;
			case 3:
				AudioA.GetComponent<AudioAnalizer>().audioLevel = Resources.Load<AudioLevel>("AudioLevels/Nordic");
				AudioA.GetComponent<AudioSource>().clip = FxNordic;
				break;
			case 6:
				AudioA.GetComponent<AudioAnalizer>().audioLevel = Resources.Load<AudioLevel>("AudioLevels/ChikenRuneTheme");
				AudioA.GetComponent<AudioSource>().clip = FxChikenRune;
				break;
			case 7:
				AudioA.GetComponent<AudioAnalizer>().audioLevel = Resources.Load<AudioLevel>("AudioLevels/Japan");
				AudioA.GetComponent<AudioSource>().clip = FxJapan;
				break;
		}
		GameObject.Find ("AudioAnalizer").GetComponent<AudioAnalizer>().Play();
		//Label musique en cours
		StartCoroutine(AfficherMusique(numMusique));
	}

	IEnumerator AfficherMusique(int musiqueEnCours)
	{
		string nomMusique = "";
		LabelMusique.SetActive(true);
		LabelMusique.transform.FindChild("Label").GetComponent<Text>().text = variablesGlobales.tabMusics[musiqueEnCours - 1]; 
		yield return new WaitForSeconds(3f);
		LabelMusique.SetActive(false);
	}

	void Update()
	{
		if(variablesGlobales.etatJeu == "bilanBardball2")
		{
			if(Input.GetButtonUp("Submit") || Input.GetButtonUp("Submit2") || Input.GetButtonUp("Submit3") || Input.GetButtonUp("Submit4") || Input.GetButtonUp("Submit5") || Input.GetButtonUp("Submit6") || Input.GetButtonUp("Submit7") || Input.GetButtonUp("Submit8"))
			{
				variablesGlobales.etatJeu = "replay";
				GestionSuitePanneauResultat(true);
			}
			else if(Input.GetButtonUp("Cancel") || Input.GetButtonUp("Cancel2") || Input.GetButtonUp("Cancel3") || Input.GetButtonUp("Cancel4") || Input.GetButtonUp("Cancel5") || Input.GetButtonUp("Cancel6") || Input.GetButtonUp("Cancel7") || Input.GetButtonUp("Cancel8"))
			{
				variablesGlobales.etatJeu = "quit";
				GestionSuitePanneauResultat(false);
			}
		}
		else if(variablesGlobales.etatJeu == "playBardeball")
		{
			//Gestion pause
			if(((Input.GetButtonUp("Submit") || Input.GetButtonUp("Submit5")) && (Input.GetButtonUp("Cancel") || Input.GetButtonUp("Cancel5"))) ||
				((Input.GetButtonUp("Submit2") || Input.GetButtonUp("Submit6")) && (Input.GetButtonUp("Cancel2") || Input.GetButtonUp("Cancel6"))) ||
				((Input.GetButtonUp("Submit3") || Input.GetButtonUp("Submit7")) && (Input.GetButtonUp("Cancel3") || Input.GetButtonUp("Cancel7"))) ||
				((Input.GetButtonUp("Submit4") || Input.GetButtonUp("Submit8")) && (Input.GetButtonUp("Cancel4") || Input.GetButtonUp("Cancel8"))))
			{
				variablesGlobales.etatJeu = "pause";
				Time.timeScale = 0;
				GameObject.Find("AudioAnalizer").GetComponent<AudioSource>().Pause();
				PanelPause.SetActive(true);
				nbrJoueurValidatePausePlay = 0;
				nbrJoueurValidatePauseQuit = 0;
				PanelPause.transform.FindChild("Panneau").FindChild("Consigne1").FindChild("Label").GetComponent<Text>().text = "Play " + nbrJoueurValidatePausePlay + "/" + variablesGlobales.nbrPlayers;
				PanelPause.transform.FindChild("Panneau").FindChild("Consigne2").FindChild("Label").GetComponent<Text>().text = "Quit " + nbrJoueurValidatePauseQuit + "/" + variablesGlobales.nbrPlayers;
			}
		}
		else if(variablesGlobales.etatJeu == "pause")
		{
			if(Input.GetButtonUp("Submit") || Input.GetButtonUp("Submit2") || Input.GetButtonUp("Submit3") || Input.GetButtonUp("Submit4") || Input.GetButtonUp("Submit5") || Input.GetButtonUp("Submit6") || Input.GetButtonUp("Submit7") || Input.GetButtonUp("Submit8"))
			{
				nbrJoueurValidatePausePlay++;
				PanelPause.transform.FindChild("Panneau").FindChild("Consigne1").FindChild("Label").GetComponent<Text>().text = "Play " + nbrJoueurValidatePausePlay + "/" + variablesGlobales.nbrPlayers;
			}
			else if(Input.GetButtonUp("Cancel") || Input.GetButtonUp("Cancel2") || Input.GetButtonUp("Cancel3") || Input.GetButtonUp("Cancel4") || Input.GetButtonUp("Cancel5") || Input.GetButtonUp("Cancel6") || Input.GetButtonUp("Cancel7") || Input.GetButtonUp("Cancel8"))
			{
				nbrJoueurValidatePauseQuit++;
				PanelPause.transform.FindChild("Panneau").FindChild("Consigne2").FindChild("Label").GetComponent<Text>().text = "Quit " + nbrJoueurValidatePauseQuit + "/" + variablesGlobales.nbrPlayers;
			}
			//On reprend la partie
			if(nbrJoueurValidatePausePlay == variablesGlobales.nbrPlayers)
			{
				GameObject.Find("AudioAnalizer").GetComponent<AudioSource>().Play();
				PanelPause.SetActive(false);
				Time.timeScale = 1;
				variablesGlobales.etatJeu = "playBardeball";
			}
			//On quitte la partie
			else if(nbrJoueurValidatePauseQuit == variablesGlobales.nbrPlayers)
			{
				Time.timeScale = 1;
				variablesGlobales.etatJeu = "quit";
				GestionSuitePanneauResultat(false);
			}
		}
	}

	//Gestion but
	public void GestionBut(int joueurQuiMarque, int but, GameObject leBut)
	{
		if(variablesGlobales.etatJeu == "playBardeball")
		{
			//Fx but
			AudioS.GetComponent<AudioSource>().PlayOneShot(FxGoal);
			AudioS.GetComponent<AudioSource>().PlayOneShot(FxPublic);
			//Commentaire
			int numCom = (int)Random.Range(1, 6.5f);
			if(numCom == 1) AudioS.GetComponent<AudioSource>().PlayOneShot(FxGoal1);
			else if(numCom == 2) AudioS.GetComponent<AudioSource>().PlayOneShot(FxGoal2);
			else if(numCom == 3) AudioS.GetComponent<AudioSource>().PlayOneShot(FxGoal3);
			else if(numCom == 4) AudioS.GetComponent<AudioSource>().PlayOneShot(FxGoal4);
			else if(numCom == 5) AudioS.GetComponent<AudioSource>().PlayOneShot(FxGoal5);
			else if(numCom == 6) AudioS.GetComponent<AudioSource>().PlayOneShot(FxGoal6);
			//Clignotement
			StartCoroutine(ClignoterBut(leBut));
			//Variable locale
			int scoreTemp = 0;
			//MaJ du score
			if(joueurQuiMarque == 1) scoreTemp = variablesGlobales.goalP1 += but;
			else if(joueurQuiMarque == 2) scoreTemp = variablesGlobales.goalP2 += but;
			else if(joueurQuiMarque == 3) scoreTemp = variablesGlobales.goalP3 += but;
			else if(joueurQuiMarque == 4) scoreTemp = variablesGlobales.goalP4 += but;
			//Interface
			Score.transform.FindChild("P" + joueurQuiMarque).FindChild("Score").GetComponent<Text>().text = scoreTemp.ToString();
			LabelInfo3.GetComponent<Text>().text = "P" + joueurQuiMarque + " GOAL!";
			//Controle de fin
			if(variablesGlobales.goalP1 >= variablesGlobales.goalsToWin || variablesGlobales.goalP2 >= variablesGlobales.goalsToWin || variablesGlobales.goalP3 >= variablesGlobales.goalsToWin || variablesGlobales.goalP4 >= variablesGlobales.goalsToWin)
			{
				variablesGlobales.etatJeu = "bilanBardball";
				Invoke("ControleFin", 1.5f);
			}
			else
			{
				//Potentiel soutient du public
				RandomCriDuPublic(joueurQuiMarque);
				Invoke("EffacerLabelBut", 1.5f);
			}
		}
	}
	private void ControleFin()
	{
		//Fin
		GameObject.Find("But").SetActive(false);
		GameObject.Find("But2").SetActive(false);
		MiniBut.SetActive(false);
		LabelInfo3.GetComponent<Text>().text = "";
		LabelInfo1.GetComponent<Text>().text = "Victory!";
		//Fx
		GameObject.Find ("AudioAnalizer").GetComponent<AudioAnalizer>().Stop ();
		AudioS.GetComponent<AudioSource>().PlayOneShot (FxResultat);
		Invoke ("LancerPanneauResultat", 2);
	}
	private void EffacerLabelBut()
	{
		LabelInfo3.GetComponent<Text>().text = "";
	}

	//Clignotement but
	IEnumerator ClignoterBut(GameObject but)
	{
		//Debut tremblement fond
		Fond.GetComponent<Animator>().speed = 60;
		//Tremblement cercle
		but.SetActive(false);
		yield return new WaitForSeconds(0.1f);
		but.SetActive(false);
		yield return new WaitForSeconds(0.1f);
		but.SetActive(true);
		yield return new WaitForSeconds(0.1f);
		but.SetActive(false);
		yield return new WaitForSeconds(0.1f);
		but.SetActive(true);
		yield return new WaitForSeconds(0.1f);
		but.SetActive(false);
		yield return new WaitForSeconds(0.1f);
		but.SetActive(true);
		yield return new WaitForSeconds(0.1f);
		but.SetActive(false);
		yield return new WaitForSeconds(0.1f);
		but.SetActive(true);
		yield return new WaitForSeconds(0.1f);
		but.SetActive(false);
		yield return new WaitForSeconds(0.1f);
		but.SetActive(true);
		//Fin tremblement fond
		Fond.GetComponent<Animator>().speed = 1;
	}

	public void RandomCriDuPublic(int numJoueur)
	{
		//Potentiel soutient du public
		int soutientPublic = (int)Random.Range(1, 4.5f);
		if(soutientPublic == 3)
		{
			if(numJoueur == 1) StartCoroutine(CriDuPublic(variablesGlobales.warriorP1));
			else if(numJoueur == 2) StartCoroutine(CriDuPublic(variablesGlobales.warriorP2));
			else if(numJoueur == 3) StartCoroutine(CriDuPublic(variablesGlobales.warriorP3));
			else if(numJoueur == 4) StartCoroutine(CriDuPublic(variablesGlobales.warriorP4));
		}
	}

	//Public en folie
	private IEnumerator CriDuPublic(int numDieu)
	{
		//Attente commentaire
		float attenteCom = (int)Random.Range(2, 4.5f);
		yield return new WaitForSeconds(attenteCom);
		if(variablesGlobales.etatJeu == "playBardeball")
		{
			if(numDieu == 1) AudioS.GetComponent<AudioSource>().PlayOneShot(FxPublicAnubis);
			else if(numDieu == 2) AudioS.GetComponent<AudioSource>().PlayOneShot(FxPublicHuitzi);
			else if(numDieu == 3) AudioS.GetComponent<AudioSource>().PlayOneShot(FxPublicJakuta);
			else if(numDieu == 4) AudioS.GetComponent<AudioSource>().PlayOneShot(FxPublicTyr);
		}
	}

	//Quand un joueur meurt
	public void GestionControleFin(int idP)
	{
		/*if(idP == 1)
		{
			P1.SetActive (false);
			variablesGlobales.vieP1 = 0;
		}
		else if(idP == 2)
		{
			P2.SetActive (false);
			variablesGlobales.vieP2 = 0;
		}
		else if(idP == 3)
		{
			P3.SetActive (false);
			variablesGlobales.vieP3 = 0;
		}
		else if(idP == 4)
		{
			P4.SetActive (false);
			variablesGlobales.vieP4 = 0;
		}
		nbrJoueurs--;
		//Fin du jeu
		if(nbrJoueurs == 1)
		{
			LabelInfo1.GetComponent<Text> ().text = "Victory!";
			GameObject.Find ("Pieges").SetActive (false);
			//Fx
			GameObject.Find ("AudioAnalizer").GetComponent<AudioAnalizer>().Stop ();
			AudioS.GetComponent<AudioSource>().PlayOneShot (FxResultat);
			Invoke ("LancerPanneauResultat", 2);
		}*/
	}

	//Afficher panneau resultat
	private void LancerPanneauResultat()
	{
		print(variablesGlobales.goalP1 + " / " + variablesGlobales.goalP2 + " / " + variablesGlobales.goalP3 + " / " + variablesGlobales.goalP4);
		LabelInfo1.GetComponent<Text> ().text = "";
		PanelResultat.SetActive(true);
		//Vainqueur
		List<int> ListePerdants = new List<int>();
		int vainqueur = 1;
		int goalTemp = variablesGlobales.goalP1;
		if(variablesGlobales.goalP2 > goalTemp)
		{
			goalTemp = variablesGlobales.goalP2;
			ListePerdants.Add(vainqueur);
			vainqueur = 2;
		}
		else ListePerdants.Add(2);
		if(variablesGlobales.goalP3 > goalTemp)
		{
			goalTemp = variablesGlobales.goalP3;
			ListePerdants.Add(vainqueur);
			vainqueur = 3;
		}
		else ListePerdants.Add(3);
		if(variablesGlobales.goalP4 > goalTemp)
		{
			goalTemp = variablesGlobales.goalP4;
			ListePerdants.Add(vainqueur);
			vainqueur = 4;
		}
		else ListePerdants.Add(4);
		PanelResultat.transform.FindChild ("Titre").GetComponent<Text> ().text = "PLAYER " + vainqueur + " WINS!";
		//Score
		if(vainqueur == 1) variablesGlobales.scoreP1++;
		else if(vainqueur == 2) variablesGlobales.scoreP2++;
		else if(vainqueur == 3) variablesGlobales.scoreP3++;
		else if(vainqueur == 4) variablesGlobales.scoreP4++;
		//Skin du vainqueur et des perdants
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP1").FindChild("Joueur1").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP1) as Sprite;
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP2").FindChild("Joueur2").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP2) as Sprite;
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP3").FindChild("Joueur3").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP3) as Sprite;
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP4").FindChild("Joueur4").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP4) as Sprite;
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP1").FindChild("Joueur1").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 0.3f);
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP2").FindChild("Joueur2").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 0.3f);
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP3").FindChild("Joueur3").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 0.3f);
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP4").FindChild("Joueur4").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 0.3f);
		if(vainqueur == 1) PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP1").FindChild("Joueur1").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 1f);
		else if(vainqueur == 2) PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP2").FindChild("Joueur2").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 1f);
		else if(vainqueur == 3) PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP3").FindChild("Joueur3").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 1f);
		else if(vainqueur == 4) PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP4").FindChild("Joueur4").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 1f);
		//Scores totaux
		PanelResultat.transform.FindChild("Scores").FindChild("ScoreP1").GetComponent<Text> ().text = "P1\n" + variablesGlobales.scoreP1;
		PanelResultat.transform.FindChild("Scores").FindChild("ScoreP2").GetComponent<Text> ().text = "P2\n" + variablesGlobales.scoreP2;
		PanelResultat.transform.FindChild("Scores").FindChild("ScoreP3").GetComponent<Text> ().text = "P3\n" + variablesGlobales.scoreP3;
		PanelResultat.transform.FindChild("Scores").FindChild("ScoreP4").GetComponent<Text> ().text = "P4\n" + variablesGlobales.scoreP4;
		//Nombre de joueurs
		if (variablesGlobales.nbrPlayers < 3)
		{
			PanelResultat.transform.FindChild("Scores").FindChild("ScoreP3").gameObject.SetActive(false);
		}
		else {
			PanelResultat.transform.FindChild("Scores").FindChild("ScoreP3").gameObject.SetActive(true);
		}
		if (variablesGlobales.nbrPlayers < 4)
		{
			PanelResultat.transform.FindChild("Scores").FindChild("ScoreP4").gameObject.SetActive(false);
		}
		else {
			PanelResultat.transform.FindChild("Scores").FindChild("ScoreP4").gameObject.SetActive(true);
		}
		Invoke("ActifBoutonSuite", 2);
	}
	private void ActifBoutonSuite()
	{
		variablesGlobales.etatJeu = "bilanBardball2";
	}
	public void GestionSuitePanneauResultat(bool continuer)
	{
		if(continuer)
		{
			print("Continuer !");
			SceneManager.LoadScene("SceneBardeBall");
		}
		else if(!continuer)
		{
			print("Quitter !");
			SceneManager.LoadScene("SceneMenuV2");
		}
	}

	//Gestion des feedbacks des items
	public void FeedbackItems(Item.ItemType idItem)
	{
		switch(idItem)
		{
		case Item.ItemType.BUBBLE:
			LabelInfo2.GetComponent<Text>().text = "Bubble!";
			//Commentaire
			AudioS.GetComponent<AudioSource>().PlayOneShot(FxItemBubble);
			break;
		case Item.ItemType.INVINCIBILITY:
			LabelInfo2.GetComponent<Text>().text = "Shield!";
			//Commentaire
			AudioS.GetComponent<AudioSource>().PlayOneShot(FxItemShield);
			break;
		case Item.ItemType.INVERSION:
			LabelInfo2.GetComponent<Text>().text = "Inversion!";
			//Commentaire
			AudioS.GetComponent<AudioSource>().PlayOneShot(FxItemInversion);
			break;
		case Item.ItemType.FREEZE:
			LabelInfo2.GetComponent<Text>().text = "Freeze!";
			//Commentaire
			AudioS.GetComponent<AudioSource>().PlayOneShot(FxItemFreeze);
			break;
		case Item.ItemType.LIFE:
			LabelInfo2.GetComponent<Text>().text = "Life!";
			break;
		}
		//Fx item active
		AudioS.GetComponent<AudioSource>().PlayOneShot(FxItemActivated);
		Invoke("FinFeedbackItems", 1.5f);
	}
	private void FinFeedbackItems()
	{
		LabelInfo2.GetComponent<Text>().text = "";
	}
}
